// dzn-runtime -- Dezyne runtime library
//
// Copyright © 2015 Paul Hoogendijk <paul@dezyne.org>
// Copyright © 2016 Rob Wieringa <rob@dezyne.org>
// Copyright © 2018 Filip Toman <filip.toman@verum.com>
// Copyright © 2016 Rutger van Beusekom <rutger@dezyne.org>
// Copyright © 2016, 2019, 2023 Jan Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of dzn-runtime.
//
// dzn-runtime is free software: you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// dzn-runtime is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with dzn-runtime.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

#include <dzn/mem.h>
#include <assert.h>

#if (DZN_MISRA_C_2004==1)
#define DZN_MAX_SIZE 1048576
static uint8_t dzn_memory_array[DZN_MAX_SIZE];
static uint8_t *dzn_current_address = dzn_memory_array;

void *
dzn_calloc (size_t n, size_t size)
{
  uint8_t *res;
  assert (DZN_MAX_SIZE + dzn_memory_array - dzn_current_address >= n * size);
  res = dzn_current_address;

  dzn_current_address = &dzn_memory_array[n * size];
  return res;
}

void *
dzn_malloc (size_t size)
{
  return dzn_calloc ((size_t) 1, size);
}

void
dzn_free (void *ptr)
{
  /*no freeing, automated */
  return;
}

#else /* !DZN_MISRA_C_2004 */
#include <stdlib.h>
#include <stdio.h>

void *
dzn_calloc (size_t n, size_t size)
{
  void *res;
  res = calloc (n, size);
  if (res == (void *)0)
    assert (0);
  return res;
}

void *
dzn_malloc (size_t size)
{
  return dzn_calloc (1, size);
}

void
dzn_free (void *ptr)
{
  free (ptr);
}
#endif /* DZN_MISRA_C_2004 */
