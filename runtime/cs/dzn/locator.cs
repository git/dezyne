// dzn-runtime -- Dezyne runtime library
//
// Copyright © 2016, 2019, 2020 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
// Copyright © 2017, 2018, 2019, 2021 Rutger van Beusekom <rutger@dezyne.org>
//
// This file is part of dzn-runtime.
//
// dzn-runtime is free software: you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// dzn-runtime is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with dzn-runtime.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

using System;
using System.Collections.Generic;

namespace dzn {
    public class Locator {
        public class Services : Dictionary<String, Object> {public Services(){}public Services(Services o):base(o) {}};
        Services services;
        public Locator():this(new Services()) {}
        public Locator(Services services) {this.services = services;}
        public static String key(Type c, String key) {
            return c.Name + key;
        }
        public static String key(Object o, String key) {
            return Locator.key(o.GetType(), key);
        }
        public Locator set(Object o, String key="") {
            services[Locator.key(o,key)] = o;
            return this;
        }
        public R get<R>(String key="") {
            return (R)services[Locator.key(typeof(R), key)];
        }
        public R try_get<R>(String key="") where R: class {
            return services.ContainsKey(Locator.key(typeof(R), key)) ? get<R>() : (R) null;
        }
        public Locator clone() {return new Locator(new Services(services));}
    }
}
