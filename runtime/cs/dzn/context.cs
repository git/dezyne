// dzn-runtime -- Dezyne runtime library
//
// Copyright © 2017, 2019, 2020, 2022 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
// Copyright © 2017, 2018, 2019, 2021, 2022 Rutger van Beusekom <rutger@dezyne.org>
//
// This file is part of dzn-runtime.
//
// dzn-runtime is free software: you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// dzn-runtime is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with dzn-runtime.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

using System;
using System.Reflection;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;

namespace dzn
{
  public class runtime_error : SystemException {
    public runtime_error(String msg) : base(msg) {}
  }
  public class logic_error : runtime_error {
    public logic_error(String msg) : base(msg) {}
  }
  public class forced_unwind: runtime_error
  {
    public forced_unwind(): base("forced_unwind") {}
  };
  public class context : IDisposable
  {
    enum State {INITIAL, RELEASED, BLOCKED, FINAL};
    String to_string(State state)
    {
      switch(state)
      {
      case State.INITIAL: return "INITIAL";
      case State.RELEASED: return "RELEASED";
      case State.BLOCKED: return "BLOCKED";
      case State.FINAL: return "FINAL";
      }
      throw new logic_error("UNKNOWN STATE");
    }

    State state;
    Action rel;
    Action<Action<context>> work;
    Thread thread;
    public context()
    {
      this.state = State.INITIAL;
      this.work = null;
      this.thread = new Thread(() =>
        {
          try
          {
            context.lck(this, () => {
              while(this.state != State.FINAL)
              {
                do_block(this);
                if(this.state == State.FINAL) break;
                if(this.work == null) break;
                Monitor.Exit(this);
                this.work((c) => {this.yield(c);});
                Monitor.Enter(this);
                if(state == State.FINAL) break;
                if(this.rel != null) this.rel();
              }
              });
          }
          catch (forced_unwind) {
            Debug.WriteLine("ignoring forced_unwind");
          }
        });
      this.thread.Start();
      context.lck(this, () => {
          while(state != State.BLOCKED) Monitor.Wait(this);
        });
    }
    public context(bool b)
    {
    }
    public context(Action<Action<context>> work) : this ()
    {
      context.lck(this, () => {
          this.work = work;
        });
    }
    ~context()
    {
      Dispose(false);
    }
    protected virtual void Dispose(bool gc)
    {
      if (gc)
      {
        context.lck(this, () => {
            do_finish(this);
            rel = null;
            work = null;
            thread = null;
          });
      }
    }
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }
    public void finish()
    {
      context.lck(this, () => {
          do_finish(this);
        });
    }
    public void block()
    {
      context.lck(this, () => {
          do_block(this);
        });
    }
    public void release()
    {
      context.lck(this, () => {
          do_release(this);
        });
    }
    public void call(context c)
    {
      context.lck(this, () => {
          do_release(this);

          Monitor.Enter(c);
          c.state = State.BLOCKED;
        });

      do { Monitor.Wait(c); } while(c.state == State.BLOCKED);
      Monitor.Exit(c);
    }
    public void yield(context to)
    {
      if(to == this) return;
      context.lck(this, () => {
          to.release();
          do_block(this);
        });
    }
    private void do_block(Object mutex)
    {
      state = State.BLOCKED;
      Monitor.Pulse(mutex);
      do { Monitor.Wait(mutex); } while(state == State.BLOCKED);
      if(state == State.FINAL) throw new forced_unwind();
    }
    private void do_release(Object mutex)
    {
      if(state != State.BLOCKED)
        throw new runtime_error("context is not allowed to release a call which is "
                                + to_string(state));
      state = State.RELEASED;
      Monitor.Pulse(mutex);
    }
    private void do_finish(Object mutex)
    {
      state = State.FINAL;
      Monitor.PulseAll(mutex);
      Monitor.Exit(mutex);
      System.Diagnostics.Debug.Assert(this.thread != null);
      this.thread.Join();
    }
    public static void lck(object o, Action f) {
      Monitor.Enter(o);
      try { f(); }
      finally { if(Monitor.IsEntered(o)) Monitor.Exit(o); }
    }
    public static T lck<T>(object o, Func<T> f) {
      Monitor.Enter(o);
      try { return f(); }
      finally { if(Monitor.IsEntered(o)) Monitor.Exit(o); }
    }
  };
}
