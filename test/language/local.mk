# Dezyne --- Dezyne command line tools
#
# Copyright © 2020,2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
#
# This file is part of Dezyne.
#
# Dezyne is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Dezyne is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
#
# Commentary:
#
# Code:

EXTRA_DIST +=					\
 %D%/README					\
 %D%/component0.dzn				\
 %D%/component1.dzn				\
 %D%/component1a.dzn				\
 %D%/component1b.dzn				\
 %D%/component1c.dzn				\
 %D%/component1d.dzn				\
 %D%/component2.dzn				\
 %D%/component2a.dzn				\
 %D%/component2b.dzn				\
 %D%/component2c.dzn				\
 %D%/component3.dzn				\
 %D%/component3a.dzn				\
 %D%/component4.dzn				\
 %D%/component5.dzn				\
 %D%/component5a.dzn				\
 %D%/component6.dzn				\
 %D%/component6a.dzn				\
 %D%/component6b.dzn				\
 %D%/component7.dzn				\
 %D%/component8.dzn				\
 %D%/component9.dzn				\
 %D%/component9a.dzn				\
 %D%/component10.dzn				\
 %D%/component10a.dzn				\
 %D%/component11.dzn				\
 %D%/component11a.dzn				\
 %D%/component12.dzn				\
 %D%/component12a.dzn				\
 %D%/component-behavior.dzn			\
 %D%/component-bool.dzn				\
 %D%/component-data.dzn				\
 %D%/component-empty.dzn			\
 %D%/component-enum.dzn				\
 %D%/component-enum-local.dzn			\
 %D%/component-enum-member.dzn			\
 %D%/component-int.dzn				\
 %D%/component-incomplete-action.dzn		\
 %D%/component-incomplete-port.dzn		\
 %D%/component-on.dzn				\
 %D%/component-provides.dzn			\
 %D%/component-requires.dzn			\
 %D%/component-state.dzn			\
 %D%/enum-variable-expression-missing.dzn	\
 %D%/import-nonexistent.dzn			\
 %D%/import/import.dzn				\
 %D%/import/lib/lib.dzn				\
 %D%/interface-behavior.dzn			\
 %D%/interface0.dzn				\
 %D%/interface1.dzn				\
 %D%/interface1b.dzn				\
 %D%/interface2.dzn				\
 %D%/interface3.dzn				\
 %D%/interface4.dzn				\
 %D%/interface5.dzn				\
 %D%/interface5b.dzn				\
 %D%/interface6.dzn				\
 %D%/interface6a.dzn				\
 %D%/interface7.dzn				\
 %D%/interface8.dzn				\
 %D%/interface8a.dzn				\
 %D%/interface8b.dzn				\
 %D%/interface9.dzn				\
 %D%/interface9a.dzn				\
 %D%/interface9b.dzn				\
 %D%/interface9c.dzn				\
 %D%/interface9d.dzn				\
 %D%/interface10.dzn				\
 %D%/interface10a.dzn				\
 %D%/interface11.dzn				\
 %D%/interface11a.dzn				\
 %D%/interface12.dzn				\
 %D%/interface12a.dzn				\
 %D%/interface13.dzn				\
 %D%/interface13a.dzn				\
 %D%/typo.dzn					\
 %D%/blocking.dzn				\
 %D%/deep-space-enum.dzn			\
 %D%/double.space.dzn				\
 %D%/import-double.space.dzn			\
 %D%/enum.dzn					\
 %D%/function.dzn				\
 %D%/global.space.dzn				\
 %D%/ienum.dzn					\
 %D%/ihello.dzn					\
 %D%/ihello-enum.dzn				\
 %D%/ihello-int.dzn				\
 %D%/iint.dzn					\
 %D%/ilookup.dzn				\
 %D%/illegal.dzn				\
 %D%/import.dzn					\
 %D%/int.dzn					\
 %D%/interface-enum.dzn				\
 %D%/lookup.dzn					\
 %D%/namespace-enum.dzn				\
 %D%/partial-enum-literal.dzn			\
 %D%/partial-if-expression.dzn			\
 %D%/partial-trigger-name.dzn			\
 %D%/partial-type-name.dzn			\
 %D%/space-hello.dzn				\
 %D%/space-ihello.dzn				\
 %D%/system.dzn					\
 %D%/system-binding.dzn				\
 %D%/variable-incomplete.dzn
