# Dezyne --- Dezyne command line tools
#
# Copyright © 2016 Rob Wieringa <rma.wieringa@gmail.com>
# Copyright © 2016, 2018, 2020, 2021, 2022, 2023 Rutger van Beusekom <rutger@dezyne.org>
# Copyright © 2016, 2017, 2018, 2019, 2020, 2021, 2023 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
# Copyright © 2020 Johri van Eerd <vaneerd.johri@gmail.com>
#
# This file is part of Dezyne.
#
# Dezyne is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Dezyne is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
#
# Commentary:
#
# Code:

include config.make

.PHONY: default test

default: $(OUT)/test

define CHECKPARAM
ifeq ($(origin $(1)), undefined)
$$(error $(1) undefined)
endif
endef

$(foreach i,IN OUT,$(eval $(call CHECKPARAM,$(i))))

SHELL:=bash
CCACHE:=$(shell type -p ccache)
CXX:=$(CCACHE) g++
ifndef WARN_FLAGS
WARN_FLAGS=					\
 -Wall						\
 -Wextra					\
 -Werror					\
 -Wshadow
endif
NOWARN_FLAGS=					\
 -Wno-unused-variable				\
 -Wno-unused-parameter				\
 -Wno-unused-but-set-variable
DEPEND_CXXFLAGS = -MMD -MF $(@:%.o=%.d) -MT '$(@:%.o=%.d) $@'
TEST_CXXFLAGS = $(DEPEND_CXXFLAGS) $(PTHREAD_CFLAGS) $(WARN_FLAGS)
INCLUDES = -I$(OUT)				\
 -I$(OUT)/..					\
 -I$(OUT)/../..					\
 -I$(OUT)/../../c++				\
 -I$(IN) -I$(IN)/..				\
 -I$(abs_top_srcdir)/runtime/c++
CPPFLAGS = $(INCLUDES) $(DEFINES)
LDFLAGS =						\
 -Wl,-rpath -Wl,$(abs_top_builddir)/runtime/.libs/	\
  $(abs_top_builddir)/runtime/.libs/libdzn-c++.so	\
  $(LIBBOOST_COROUTINE)
GLOBALS_H=$(wildcard $(IN)/globals.h)
ifneq ($(GLOBALS_H),)
CPPFLAGS:=$(CPPFLAGS) -include $(GLOBALS_H)
endif
CALLING_CONTEXT_HH=$(wildcard $(IN)/c++/calling_context.hh)
ifneq ($(CALLING_CONTEXT_HH),)
CPPFLAGS:=$(CPPFLAGS) -include $(CALLING_CONTEXT_HH)
endif

$(OUT)/%.o: $(IN)/%.cc
	mkdir -p $(dir $@)
	$(COMPILE.cc) $(TEST_CXXFLAGS) -o $@ $<

$(OUT)/%.o: $(IN)/c++/%.cc
	mkdir -p $(dir $@)
	$(COMPILE.cc) $(TEST_CXXFLAGS) -o $@ $<

$(OUT)/%.o: $(OUT)/%.cc
	mkdir -p $(dir $@)
	$(COMPILE.cc) $(TEST_CXXFLAGS) $(NOWARN_FLAGS) -o $@ $<

$(foreach f, $(wildcard $(IN)/c++/*.cc), $(eval $(OUT)/test: $(patsubst $(IN)/c++/%.cc, $(OUT)/%.o, $(f))))

$(OUT)/test: $(patsubst $(IN)/%.cc, $(OUT)/%.o, $(wildcard $(IN)/*.cc))
$(OUT)/test: $(patsubst $(OUT)/%.cc, $(OUT)/%.o, $(wildcard $(OUT)/*.cc))
$(OUT)/test: $(patsubst %.cc, %.o,$(wildcard $(OUT)/*.cc))
$(OUT)/test: $(patsubst %.cpp, %.o,$(wildcard $(OUT)/*.cpp))
$(OUT)/test: $(MAIN_O) $(abs_top_builddir)/runtime/.libs/libdzn-c++.so
	mkdir -p $(dir $@)
	$(LINK.cc) $(TEST_CXXFLAGS) -o $@ $^ $(LDFLAGS)

-include $(patsubst $(IN)/%.cc, $(OUT)/%.d, $(wildcard $(IN)/*.cc))
