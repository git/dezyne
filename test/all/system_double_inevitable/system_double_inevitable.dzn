// Dezyne --- Dezyne command line tools
//
// Copyright © 2019, 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
// Copyright © 2022 Rutger van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  in void hello ();
  in void bye ();
  out void cruel ();
  out void world ();

  behavior
  {
    bool idle = true;
    [idle] on hello: idle = false;
    [idle] on bye: {}
    [!idle] on inevitable: {idle = true; cruel; world;}
  }
}

component hello
{
  provides ihello h;
  requires ihello w;

  behavior
  {
    bool idle = true;
    [idle] on h.hello (): {idle = false; w.hello ();}
    [idle] on h.bye (): {}
    [!idle] on h.bye (): illegal;
    on w.cruel (): {h.cruel (); w.bye();}
    on w.world (): {idle = true; h.world ();}
  }
}

component system_double_inevitable
{
  provides ihello h;
  requires ihello w;

  system
    {
      h <=> top.h;
      hello top;
      top.w <=> bottom.h;
      hello bottom;
      bottom.w <=> w;
    }
}
