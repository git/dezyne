// Dezyne --- Dezyne command line tools
//
// Copyright © 2022 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// This component starts in either the pressed or the released state,
// then with time it repeatedly toggles its state.
//
// Code:

interface ihello
{
  enum status {A, B, C};
  in status hello ();
  in void bye ();

  behavior
  {
    status s = status.A;

    [s.A]
    {
      on hello: {s = status.B; reply (s);}
      on hello: {s = status.C; reply (s);}
      on bye: {}
    }
    [!s.A] on bye: s = status.A;
  }
}

component constraint_nondet
{
  provides ihello h;
  requires ihello w;

  behavior
  {
    on h.hello (): reply (w.hello ());
    on h.bye (): w.bye ();
  }
}
