// Dezyne --- Dezyne command line tools
//
// Copyright © 2019, 2020, 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  in void hello();

  behavior
  {
    on hello: {}
  }
}

component Foreign
{
  provides iworld w;
  // behavior {}
}

component hello
{
  provides ihello h;
  requires iworld left;
  requires iworld right;

  behavior
  {
    on h.hello(): {left.world(); right.world();}
  }
}

interface iworld
{
  in void world();

  behavior
  {
    on world: {}
  }
}

component hello_foreign_path
{
  provides ihello h;

  system
    {
      hello c;
      Foreign left;
      Foreign right;
      h <=> c.h;
      c.left <=> left.w;
      c.right <=> right.w;
    }
}
