// Dezyne --- Dezyne command line tools
//
// Copyright © 2024 Rutger van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  in void hello ();
  out void world ();
  behavior
  {
    enum State {A,B,C};
    State state = State.A;
    [state.A] on hello: state = State.B;
    [state.B] on inevitable:
    {
      state = State.C;
      world;
      state = State.B;
      world;
      state = State.A;
    }
  }
}

component shared_guarded_trailing_assign
{
  provides ihello h;
  requires ihello w;
  behavior
  {
    [h.state.A][w.state.A] on h.hello (): w.hello ();
    [h.state.B][w.state.A] on w.world (): h.world ();
    [h.state.C][w.state.A] on w.world (): h.world ();
  }
}
