// Dezyne --- Dezyne command line tools
//
// Copyright © 2018, 2022 Rutger van Beusekom <rutger@dezyne.org>
// Copyright © 2021, 2022 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

extern int $int$;

interface ihello
{
  in bool hello (inout int i);
  in void hello_void (inout int i);

  behavior
  {
    on hello: reply (true);
    on hello_void: {}
  }
}

interface iworld
{
  in void hello ();
  out void world ();
  in void hello_void ();
  out void world_void ();

  behavior
  {
    on hello: world;
    on hello_void: world_void;
  }
}

component sync_out_reply_binding
{
  provides ihello h;
  requires iworld w;

  behavior
  {
    int g = $123$;

    on h.hello (n <- g): w.hello ();
    on w.world (): {g = $456$; h.reply (true); g = $789$;}

    on h.hello_void (n <- g): w.hello_void ();
    on w.world_void (): g = $456$;
  }
}
