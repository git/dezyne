// Dezyne --- Dezyne command line tools
//
// Copyright © 2014, 2015 Henk Katerberg <hank@mudball.nl>
// Copyright © 2016 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
// Copyright © 2017 Rob Wieringa <rma.wieringa@gmail.com>
// Copyright © 2022 Rutger van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published byo the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

//import BallSorter.dzn;
import blackboxBallSorter.dzn;
//import Brick1.dzn;
import blackboxBrick1.dzn;
//import Brick2.dzn;
import blackboxBrick2.dzn;
//import Brick3.dzn;
import blackboxBrick3.dzn;
//import Brick4.dzn;
import blackboxBrick4.dzn;

import IHandle.dzn;

component LegoBallSorter
{
  provides IHandleRobust ctrl;
  requires imotor brick1_aA;
  requires imotor brick1_aB;
  requires imotor brick1_aC;
  requires itouch brick1_s1;
  requires itouch brick1_s2;
  requires itouch brick1_s3;
  requires itouch brick1_s4;

  requires imotor brick2_aA;
  requires imotor brick2_aB;
  requires itouch brick2_s2;
  requires itouch brick2_s3;
  requires itouch brick2_s4;

  requires imotor brick3_aA;
  requires imotor brick3_aC;
  requires ilight brick3_s1;
  requires itouch brick3_s2;
  requires itouch brick3_s3;

  requires imotor brick4_aA;
  requires imotor brick4_aB;
  requires imotor brick4_aC;
  requires itouch brick4_s1;
  requires itouch brick4_s2;
  requires itouch brick4_s3;

  system
  {
    BallSorter sorter;

    ctrl <=> sorter.ctrl;

    Brick1 brick1;

    sorter.move1 <=> brick1.motorA;
    sorter.move2 <=> brick1.motorB;
    sorter.move3 <=> brick1.motorC;

    sorter.touch1 <=> brick1.touch1;
    sorter.touch2 <=> brick1.touch2;
    sorter.touch3 <=> brick1.touch3;
    sorter.touch4 <=> brick1.touch4;

    brick1.aA <=> brick1_aA;
    brick1.aB <=> brick1_aB;
    brick1.aC <=> brick1_aC;

    brick1.s1 <=> brick1_s1;
    brick1.s2 <=> brick1_s2;
    brick1.s3 <=> brick1_s3;
    brick1.s4 <=> brick1_s4;

    Brick2 brick2;

    sorter.move4 <=> brick2.motorA;
    sorter.move5 <=> brick2.motorB;

    sorter.touch6 <=> brick2.touch6;
    sorter.touch7 <=> brick2.touch7;
    sorter.touch8 <=> brick2.touch8;

    brick2.aA <=> brick2_aA;
    brick2.aB <=> brick2_aB;

    brick2.s2 <=> brick2_s2;
    brick2.s3 <=> brick2_s3;
    brick2.s4 <=> brick2_s4;

    Brick3 brick3;

    sorter.move6 <=> brick3.motorA;
    sorter.move7 <=> brick3.motorC;

    sorter.light1 <=> brick3.light1;
    sorter.touch9 <=> brick3.touch9;
    sorter.touch10 <=> brick3.touch10;

    brick3.aA <=> brick3_aA;
    brick3.aC <=> brick3_aC;

    brick3.s1 <=> brick3_s1;
    brick3.s2 <=> brick3_s2;
    brick3.s3 <=> brick3_s3;

    Brick4 brick4;

    sorter.move8 <=> brick4.motorA;
    sorter.move9 <=> brick4.motorB;
    sorter.move10 <=> brick4.motorC;

    sorter.touch11 <=> brick4.touch11;
    sorter.touch12 <=> brick4.touch12;
    sorter.touch13 <=> brick4.touch13;

    brick4.aA <=> brick4_aA;
    brick4.aB <=> brick4_aB;
    brick4.aC <=> brick4_aC;

    brick4.s1 <=> brick4_s1;
    brick4.s2 <=> brick4_s2;
    brick4.s3 <=> brick4_s3;
  }
}
