// Dezyne --- Dezyne command line tools
//
// Copyright © 2014, 2015 Henk Katerberg <hank@mudball.nl>
// Copyright © 2016, 2021, 2022 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
// Copyright © 2016 Paul Hoogendijk <paul@dezyne.org>
// Copyright © 2022, 2023 Rutger van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

import lego.dzn;
import IMove.dzn;

component Motor
{
  provides IMove ctrl;
  requires imotor hw;

  system
  {
    timer pollTimer;
    Move bhv;

    ctrl <=> bhv.ctrl;
    bhv.timer <=> pollTimer.port;
    bhv.motor <=> hw;
  }
}

component Move
{
  provides IMove ctrl;
  requires itimer timer;
  requires imotor motor;

  behavior
  {
    itimer.milliseconds_t time = $(config.get("Duration::PollInterval"))$;
    imotor.position_t destination;

    on ctrl.move(power, position): {
        destination = position;
        timer.create(time);
        motor.move(power, destination);
      }
    on ctrl.zero(): motor.zero();

    on timer.timeout(): {
      imotor.result_t arrive = motor.at(destination);

      if (arrive == imotor.result_t.yes) ctrl.arrived();
      else                               timer.create(time);
    }
    on ctrl.run(power, invert): motor.run(power, invert);
    on ctrl.stop(): {motor.stop(); timer.cancel();}
    on ctrl.coast(): {motor.coast(); timer.cancel();}
    on ctrl.position(pos): motor.position(pos);
  }
}
