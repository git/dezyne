// Dezyne --- Dezyne command line tools
//
// Copyright © 2022 Rutger van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// The inevitable return for b.world must not mistakenly reopen the
// blocking race window.
//
// Code:

interface ihello
{
  in void hello();
  out void world();
  behavior
  {
    bool idle = true;
    [idle] on hello: {idle = false;}
    [!idle] on inevitable: {idle = true; world;}
  }
}

component blocking_race
{
  provides blocking ihello p;
  requires ihello r;
  requires blocking ihello b;
  behavior
  {
    enum S {A,B,C};
    S s = S.A;
    [s.A] on p.hello(): {s = S.B; r.hello(); b.hello();}
    [s.B] on r.world(),b.world(): s = S.C;
    [s.C] on r.world(),b.world(): {s = S.A; p.world();}
  }
}
