// Dezyne --- Dezyne command line tools
//
// Copyright © 2022 Rutger (regtur) van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

extern Data $int$;
interface ihellocruelworld
{
  in void hello (Data data1, Data data2);
  in void cruel ();
  out void world (Data data1, Data data2);
  behavior
  {
    bool idle = true;
    [idle] on hello: idle = false;
    on cruel: idle = true;
    [!idle] on inevitable: {idle = true; world;}
  }
}

component defer_shadow
{
  provides ihellocruelworld h;
  behavior
  {
    bool idle = true;
    bool shadow = false;
    [idle] on h.hello (data1, data2): {
      idle = false;
      bool shadow = true;
      bool local = true;
      defer {idle = true; if(shadow && local) h.world (data1, data2);}
    }
    on h.cruel (): idle = true;
  }
}