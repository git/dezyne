// Dezyne --- Dezyne command line tools
//
// Copyright © 2021, 2023 Rutger (regtur) van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Test the injection of a foreign component.
//
// Code:

import iworld.dzn;

interface ihello
{
  in void hello ();

  behavior
  {
    on hello: {}
  }
}

component Foreign
{
  provides iworld w;
}

interface ilog
{
  in void log ();
  behavior
  {
    on log: {}
  }
}

component Log
{
  provides ilog log;
  behavior
  {
    on log.log (): {}
  }
}

component hello
{
  provides ihello h;
  requires injected iworld w;
  requires injected ilog l;

  behavior
  {
    on h.hello (): {l.log(); bool b = w.hello ();}
  }
}

component injected_foreign
{
  provides ihello h;

  system
  {
    h <=> c.h;
    hello c;
    Foreign f;
    f.w <=> *;
    Log log;
    log.log <=> *;
  }
}
