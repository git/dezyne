// Dezyne --- Dezyne command line tools
//
// Copyright © 2021 Rutger van Beusekom <rutger@dezyne.org>
// Copyright © 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  in void hello();
  in void bye();

  out void cruel();
  out void world();

  behavior
  {
    enum State {Idle, Ready, Busy};
    State state = State.Idle;
    bool once = true;

    [state.Idle]
    {
      on hello: {once = true; state = State.Ready;}
      on bye: illegal;
    }
    [state.Ready]
    {
      on bye: state = State.Busy;
      [once] on inevitable: {once = false; cruel;}
    }
    [state.Busy]
    {
      on inevitable: {state = State.Idle; world;}
    }
  }
}

component compliance_external_inevitable
{
  provides ihello h;
  requires external ihello w;

  behavior
  {
    enum State {Idle, Ready, Busy};
    State state = State.Idle;

    [state.Idle] on h.hello(): {state = State.Ready; w.hello();}
    [state.Ready] {
      on h.bye(): {state = State.Busy; w.bye();}
      on w.cruel(): h.cruel();
    }
    [state.Busy]
    {
      on w.cruel(): h.cruel(); // non-compliance
      //on w.cruel(): {} // compliance
      on w.world(): {state = State.Idle; h.world();}
    }
  }
}
