// Dezyne --- Dezyne command line tools
//
// Copyright © 2018, 2019, 2021, 2022 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  enum Bool {False, True};
  in Bool hello ();
  out void cruel ();
  out void world ();

  behavior
  {
    on hello: reply (Bool.True);
    on hello: {cruel; reply (Bool.True);}
    on hello: {world; reply (Bool.True);}
  }
}

component compliance_provides_enum
{
  provides ihello h;
  behavior
  {
    on h.hello (): reply (ihello.Bool.False);
  }
}
