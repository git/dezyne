// Dezyne --- Dezyne command line tools
//
// Copyright © 2022 Paul Hoogendijk <paul@dezyne.org>
// Copyright © 2022 Rutger van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  in void hello ();

  behavior
  {
    on hello: {}
  }
}

interface iworlds
{
  in void hello ();
  out void world ();

  behavior
  {
    on hello: world;
  }
}

interface iworlda
{
  in void hello ();
  out void world ();

  behavior
  {
    bool idle = false;
    on hello: idle = true;
    [idle] on inevitable: {world; idle = false;}
  }
}

component hello_blocking_asynchronous_sync_out
{
  provides blocking ihello p;
  requires iworlda ra;
  requires iworlds rs;

  behavior
  {
    bool blocked = false;
    blocking on p.hello (): {blocked = true; ra.hello ();}
    on ra.world (): rs.hello ();
    on rs.world (): {p.reply (); blocked = false;}
  }
}
