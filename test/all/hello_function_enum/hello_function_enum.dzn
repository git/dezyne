// Dezyne --- Dezyne command line tools
//
// Copyright © 2018, 2023 Rutger van Beusekom <rutger@dezyne.org>
// Copyright © 2018, 2019, 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Return an enum literal from a function.
//
// Code:

enum r {f,t};

interface ihello
{
  in r hello();
  in r cruel();
  in void world();

  behavior
  {
    on hello: reply (r.t);
    on cruel: reply (r.f);
    on world: {}
  }
}

component hello_function_enum
{
  provides ihello h;

  behavior
  {
    r m = r.f;
    on h.hello(): h.reply(f());
    on h.cruel(): h.reply(m);
    on h.world(): r v = f();
    r f(){ return r.t; }
  }
}
