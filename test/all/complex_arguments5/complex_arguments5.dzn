// Dezyne --- Dezyne command line tools
//
// Copyright © 2022, 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:
interface ihello
{
  in void hello ();
  out void one ();
  out void two ();
  out void three ();
  out void foo ();
  out void bar ();

  behavior
  {
    on hello: {one;foo;two;foo;three;bar;}
  }
}

component complex_arguments5
{
  provides ihello h;
  behavior
  {
    bool one () {h.one ();return false;}
    bool two () {h.two ();return false;}
    bool three () {h.three ();return true;}

    bool foo (bool x) {h.foo ();return !x;}
    void bar (bool x) {h.bar ();}

    on h.hello ():
      bar ((foo (one ()) && !foo (two ())) || !three ());
  }
}
