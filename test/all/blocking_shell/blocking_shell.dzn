// Dezyne --- Dezyne command line tools
//
// Copyright © 2016, 2017, 2022, 2023, 2024 Rutger (regtur) van Beusekom <rutger@dezyne.org>
// Copyright © 2021, 2022 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

extern int $int$;

enum Enum {OK,NOK};

subint Int {0..1};

interface ihello
{
  in void hello_void();
  in bool hello_bool();
  in Int hello_int();
  in Enum hello_enum(int i, out int j);
  out void world(int i);

  behavior
  {
    on hello_void: {world;}
    on hello_bool: {world; reply(true);}
    on hello_int: {world; reply(0);}
    on hello_enum: {world; reply(Enum.OK);}
    on hello_enum: {reply(Enum.NOK);}
  }
}

interface iworld
{
  in void hello_void();
  in bool hello_bool();
  in Int hello_int();
  in Enum hello_enum(int i, out int j);
  out void world(int i);

  behavior
  {
    on hello_void: world;
    on hello_bool: {world; reply(true);}
    on hello_int: {world; reply(0);}
    on hello_enum: {world; reply(Enum.OK);}
    on hello_enum: {reply(Enum.NOK);}
  }
}

component proxy
{
  provides blocking ihello top_p;
  requires blocking ihello top_r;

  behavior
  {
    on top_p.hello_void(): {top_r.hello_void();}
    on top_p.hello_bool(): {bool b = top_r.hello_bool(); reply(b);}
    on top_p.hello_int(): {Int i = top_r.hello_int(); reply(i);}
    on top_p.hello_enum(i,j): {Enum r = top_r.hello_enum(i,j); reply(r);}
    on top_r.world(i): top_p.world(i);
  }
}

component blocked
{
  provides blocking ihello bottom_p;
  requires external iworld bottom_r;

  behavior
  {
    enum TYPE {VOID, BOOL, INT, ENUM};
    TYPE type = TYPE.VOID;

    bool b = true;
    Int i_ = 1;
    Enum r = Enum.NOK;

    on bottom_p.hello_void(): blocking {type = TYPE.VOID; bottom_r.hello_void();}
    on bottom_p.hello_bool(): blocking {type = TYPE.BOOL; b = bottom_r.hello_bool();}
    on bottom_p.hello_int(): blocking {type = TYPE.INT; i_ = bottom_r.hello_int();}
    on bottom_p.hello_enum(i,j): blocking {type = TYPE.ENUM; r = bottom_r.hello_enum(i,j); if(r.NOK) reply(r);}
    [type.VOID] on bottom_r.world(i): {bottom_p.world(i); bottom_p.reply();}
    [type.BOOL] on bottom_r.world(i): {bottom_p.world(i); bottom_p.reply(true);}
    [type.INT]  on bottom_r.world(j): {bottom_p.world(j); bottom_p.reply(i_); i_ = 1;}
    [type.ENUM] on bottom_r.world(j): {bottom_p.world(j); bottom_p.reply(r);}
  }
}

component blocking_shell
{
  provides blocking ihello p;
  requires external iworld r;

  system
  {
    proxy top;
    blocked bottom;
    p <=> top.top_p;
    top.top_r <=> bottom.bottom_p;
    bottom.bottom_r <=> r;
  }
}
