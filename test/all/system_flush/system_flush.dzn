// Dezyne --- Dezyne command line tools
//
// Copyright © 2021 Paul Hoogendijk <paul@dezyne.org>
// Copyright © 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello {
  in void hello();
  out void world();

  behavior {
    on hello: world;
  }
}

component C {
  provides ihello hh;
  requires ihello ww;

  behavior {
    bool idle = true;

    [idle] {
      on hh.hello(): {
        ww.hello();
        idle = false;
      }
    }
    [!idle] {
      on ww.world(): {
        hh.world();
        idle = true;
      }
    }
  }
}

component system_flush {
  provides ihello h;
  requires ihello w;

  system {
    C c;
    C d;

    h <=> c.hh;
    c.ww <=> d.hh;
    d.ww <=> w;
  }
}
