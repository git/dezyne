// Dezyne --- Dezyne command line tools
//
// Copyright © 2018, 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
// Copyright © 2022 Rutger van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface I1
{
  in void e();

  behavior
  {
    on e: {}
  }
}

interface I2
{
  out void f();

  behavior
  {
    on inevitable: f;
  }
}

component A
{
  provides blocking I1 p;
  requires blocking I1 r1;
  requires blocking I2 r2;

  behavior
  {
    on p.e(): r1.e();
    on r2.f(): r1.e();
  }
}

interface I3
{
  in void e();
  out void f();

  behavior
  {
    bool pending = false;
    [!pending]
    {
      on e: pending = true;
    }
    [otherwise]
    {
      on inevitable: {pending = false; f;}
    }
  }
}

component Blocked
{
  provides blocking I1 p;
  requires blocking I3 r;

  behavior
  {
    blocking on p.e(): {r.e();}
    on r.f(): p.reply();
  }
}

component blocking_system
{
  provides blocking I1 p;
  requires blocking I2 r2;
  requires blocking I3 r3;

  system
  {
    A a;
    Blocked b;

    p <=> a.p;
    a.r1 <=> b.p;
    a.r2 <=> r2;
    b.r <=> r3;
  }
}
