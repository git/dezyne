// Dezyne --- Dezyne command line tools
//
// Copyright © 2022 Paul Hoogendijk <paul@dezyne.org>
// Copyright © 2022 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
// Copyright © 2023 Rutger (regtur) van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  in void hello ();
  in void bye ();
  out void cruel ();

  behavior
  {
    on hello: {}
    on bye: cruel;
  }
}

component compliance_blocking_function
{
  provides blocking ihello h;
  requires ihello w;
  behavior
  {
    void f ()
    {
      w.hello ();
      h.cruel ();
      h.reply ();
    }
    blocking on h.hello (): f ();
    on h.bye(): h.cruel();
  }
}
