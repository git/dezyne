// Dezyne --- Dezyne command line tools
//
// Copyright © 2023 Rutger van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// This model asserts proper shared state type resolving of State which
// exists both in the component as well as the interface.
//
// Code:

interface ihello
{
  in void hello ();
  behavior
  {
    enum State {A};
    State s = State.A;
    on hello: {}
  }
}

component shared_type_lookup
{
  provides ihello h;

  behavior
  {
    enum State {B};
    [h.s.A] on h.hello (): {}
  }
}
