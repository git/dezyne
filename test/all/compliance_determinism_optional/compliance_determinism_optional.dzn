// Dezyne --- Dezyne command line tools
//
// Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  in void hello ();
  in void bye ();
  out void cruel ();
  out void world ();

  behavior
  {
    bool idle = true;

    [idle] on hello: idle = false;
    [!idle] on bye: cruel;
    [!idle] on optional: {world; idle = true;}
  }
}

interface iworld
{
  in void hello ();
  in void bye ();
  out void cruel ();
  out void world ();

  behavior
  {
    bool idle = true;

    [idle] on hello: idle = false;
    [!idle] on bye: {cruel; idle = true;}
    [!idle] on inevitable: world;
  }
}

component compliance_determinism_optional
{
  provides ihello h;
  requires iworld w;

  behavior
  {
    on h.hello (): w.hello ();
    on h.bye (): h.cruel ();
    on w.world (): {w.bye ();}
    on w.cruel (): h.world ();
    on w.cruel (): h.cruel ();
   }
}
