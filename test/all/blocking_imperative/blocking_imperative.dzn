// Dezyne --- Dezyne command line tools
//
// Copyright © 2018, 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
// Copyright © 2022 Rutger van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface I {
  in void e();
  behavior {
    on e: {}
  }
}

interface II {
  in void e();
  out void cb();
  behavior {
    bool busy = false;
    [!busy]
       on e: busy = true;
    [busy] on inevitable: { cb; busy = false; }
  }
}

component blocking_imperative {
  provides blocking I p;
  requires II r;
  behavior {
    bool busy = false;
    [!busy] on p.e(): blocking { r.e(); busy = true; }
    [busy] on r.cb(): { p.reply(); busy = false; }
  }
}
