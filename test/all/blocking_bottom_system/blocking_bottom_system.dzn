// Dezyne --- Dezyne command line tools
//
// Copyright © 2021, 2022 Rutger van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  in void hello();

  behavior
  {
    on hello:{}
  }
}

interface iworld
{
  in void hello();
  out void world();

  behavior
  {
    bool idle = true;
    [idle] on hello: idle = false;
    [!idle]
    {
      on hello: illegal;
      on inevitable: {idle = true; world;}
    }
  }
}
component Proxy
{
  provides blocking ihello p;
  requires blocking ihello r;

  behavior
  {
    on p.hello(): r.hello();
  }
}

component Block
{
  provides blocking ihello p;
  requires blocking iworld r;

  behavior
  {
    blocking on p.hello(): r.hello();
    on r.world(): p.reply();
  }
}

component blocking_bottom_system
{
  provides blocking ihello p;
  requires blocking iworld r;

  system
  {
    Proxy proxy;
    p <=> proxy.p;

    Block block;
    proxy.r <=> block.p;
    block.r <=> r;
  }
}
