// Dezyne --- Dezyne command line tools
//
// Copyright © 2018 Rutger van Beusekom <rutger@dezyne.org>
// Copyright © 2019, 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  enum r {f,t};
  in r hello();

  behavior
  {
    on hello: reply (r.f);
  }
}

component interface_enum
{
  provides ihello h;
  requires iworld w;

  behavior
  {
    ihello.r m = ihello.r.t;
    on h.hello():
      {
        ihello.r e = ihello.r.f;
        e = w.world();
        reply (e);
      }
  }
}

interface iworld
{
  in ihello.r world();

  behavior
  {
    on world: reply (ihello.r.f);
  }
}
