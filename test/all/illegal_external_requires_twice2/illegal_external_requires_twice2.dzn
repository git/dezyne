// Dezyne --- Dezyne command line tools
//
// Copyright © 2021 Rutger van Beusekom <rutger@dezyne.org>
// Copyright © 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
interface I
{
  in void e();
  out void a();

  behavior
  {
    bool idle=true;
    [idle] on e: idle = false;
    [!idle] on inevitable: {idle = true; a;}
  }
}

interface J
{
  in void e();
  out void a();

  behavior
  {
    on e: a;
  }
}

component illegal_external_requires_twice2
{
  provides I p;
  requires external J q;
  requires external J r;
  behavior
  {
    enum S {A,B,C};
    S s = S.A;
    [s.A] on p.e(): {s = S.B; q.e(); r.e();}
    [s.B] on q.a(): {s=S.C;}
    [s.C] on r.a(): {s=S.A;p.a();}
  }
}
