// Dezyne --- Dezyne command line tools
//
// Copyright © 2018, 2021, 2022 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
// Copyright © 2018 Rutger van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  in void hello();

  out void world();

  behavior
  {
    bool idle = true;
    [idle] on hello: idle=!idle;
    [!idle] on inevitable: {idle=!idle; world;}
  }
}

component hello_multiple_provides_requires
{
  provides ihello i1;
  provides ihello i2;

  requires ihello i3;

  behavior
  {
    bool i1_idle = true;
    bool i2_idle = true;

    [i2_idle] on i1.hello(): {i1_idle = !i1_idle; i3.hello();}
    [i1_idle] on i2.hello(): {i2_idle = !i2_idle; i3.hello();}

    [!i2_idle] on i1.hello(): {i1_idle = !i1_idle;}
    [!i1_idle] on i2.hello(): {i2_idle = !i2_idle;}

    [i2_idle] on i3.world(): {i1_idle = !i1_idle; i1.world();}
    [i1_idle] on i3.world(): {i2_idle = !i2_idle; i2.world();}
    [!i1_idle && !i2_idle] on i3.world(): {i1_idle = !i1_idle; i1.world(); i3.hello();}
  }
}
