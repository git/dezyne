// Dezyne --- Dezyne command line tools
//
// Copyright © 2016, 2021, 2024 Rutger van Beusekom <rutger@dezyne.org>
// Copyright © 2020, 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  in void hello ();
  out void world ();

  behavior
  {
    bool idle = true;

    [idle]
    {
      on hello: world;
      on hello: idle = false;
    }
    [!idle] on inevitable: {idle = true; world;}
  }
}

component multiple_provides
{
  provides ihello left;
  provides ihello right;
  requires ihello r;

  behavior
  {
    bool b_left = false;
    bool b_right = false;

    [!b_left && !b_right]
    {
      on left.hello (): {r.hello (); b_left = true;}
      on right.hello ():{r.hello (); b_right = true;}
    }
    [b_left || b_right]
    {
      [!b_left] on left.hello (): left.world ();
      [!b_right] on right.hello (): right.world ();
      [b_left] on r.world (): {b_left = false; left.world ();}
      [b_right] on r.world (): {b_right = false; right.world ();}
    }
  }
}
