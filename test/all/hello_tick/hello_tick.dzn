// Dezyne --- Dezyne command line tools
//
// Copyright © 2018, 2021, 2022 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  enum r {t,f};
  in r hello();

  behavior
  {
    on hello: reply (r.f);
  }
}

component hello_tick
{
  provides ihello h;
  requires iworld w;

  behavior
  {
    enum r {f,t};

    on h.hello():
      {
        ihello.r s = w.world();
        ihello.r t = ihello.r.f;
        ihello.r u = s;
        reply (s);
      }
  }
}

interface iworld
{
  in ihello.r world();

  behavior
  {
    on world: reply (ihello.r.f);
  }
}
