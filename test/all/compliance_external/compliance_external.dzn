// Dezyne --- Dezyne command line tools
//
// Copyright © 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// The most simple external test: without external on port 'w' this
// model is fine; marking port 'w' as external, opens a race between
// provides queue h.hello and requires queue w.world.  When w.world is
// delayed by adding it to the external queue, a second h.hello triggers
// an illegal.
//
// Code:

interface ihello
{
  in void hello();
  behavior
  {
    on hello: {}
  }
}

component compliance_external
{
  provides ihello h;
  requires external iworld w;

  behavior
  {
    bool idle = true;
    [idle] on h.hello (): {idle = false; w.hello ();}
    [!idle] on w.world (): idle = true;
  }
}

interface iworld
{
  in void hello();
  out void world();
  behavior
  {
    on hello: world;
  }
}
