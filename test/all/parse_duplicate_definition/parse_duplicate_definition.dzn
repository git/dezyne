// Dezyne --- Dezyne command line tools
//
// Copyright © 2018, 2020, 2021, 2022 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

import ihello.dzn;
import hello.dzn;

extern top_float $float$;
subint int {0..1};
extern float $float$;

interface ihello
{
  in int hello(float f);

  behavior
  {
    on hello: reply(0);
  }
}

component hello
{
  provides ihello h;
  behavior
  {
    on h.hello(i): reply (0);
  }
}

component cruel
{
  provides ihello h;
  behavior
  {
    on h.hello(i): {
      bool b = false;
      bool b = false;           // duplicate
      {bool b = false;}         // shadowing
      if (true) b = false;      // shadowing
      else b = false;           // shadowing
      reply (0);
    }
  }
}

component float
{
  provides float float;
}

component ihello
{
  provides ihello ihello;
  behavior
  {
    on ihello.hello(i): reply (0);
  }
}
