// Dezyne --- Dezyne command line tools
//
// Copyright © 2018 Rob Wieringa <rma.wieringa@gmail.com>
// Copyright © 2021, 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

namespace n1.n2.n3
{
  interface I
  {
    enum Enum { N3 };
    in void e3();
    behavior { on e3: {} }
  }
}

namespace n1.n2.n3
{
  enum Enum { E3 };
}

namespace n1.n2
{
  interface I
  {
    enum Enum { N2 };
    in void e2();
    behavior { on e2: {} }
  }
}

namespace n1.n2
{
  enum Enum { E2 };
}

namespace n1
{
  interface I
  {
    enum Enum { N1 };
    in void e1();
    behavior { on e1: {} }
  }
}

namespace n1
{
  enum Enum { E1 };
}

interface I
{
  enum Enum { N0 };
  in void e0();
  behavior { on e0: {} }
}

enum Enum { E0 };

namespace n1.n2.n3
{
  component C
  {
    provides I p;
    behavior
    {
      I.Enum n = I.Enum.N3;
      Enum e = Enum.E3;
      on p.e3(): {}
    }
  }
}

namespace n1.n2
{
  component C
  {
    provides I p;
    behavior
    {
      I.Enum n = I.Enum.N2;
      Enum e = Enum.E2;
      on p.e2(): {}
    }
  }
}

namespace n1
{
  component C
  {
    provides I p;
    behavior
    {
      I.Enum n = I.Enum.N1;
      Enum e = Enum.E1;
      on p.e1(): {}
    }
  }
}

component hello_namespace_shadow2
{
  provides I p;
  requires n1.I r1;
  requires n1.n2.I r2;
  requires n1.n2.n3.I r3;
  behavior
  {
    on p.e0(): {}
  }
}
