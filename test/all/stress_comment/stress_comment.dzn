// Dezyne --- Dezyne command line tools
//
// Copyright © 2019, 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
// Copyright © 2022 Rutger van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

// b:01 begin
/* b:02 */interface/*i:02*/ihello                                  /* e:02 */
/* b:03 */{                                                        /* e:03 */
/* b:04 */  in/*i:04*/void/*i:04.1*/hello/*i:04.2*/(/*i:04.3*/)/*i:04.4*/; /* e:04 */
/* b:05 */                                                         /* e:05 */
/* b:06 */  behavior                                               /* e:06 */
/* b:07 */  {                                                      /* e:07 */
/* b:08 */    on/*i:08*/hello/*i:08.1*/:/*i:08.2*/{/*i:08.3*/}     /* e:08 */
/* b:09 */  }                                                      /* e:09 */
/* b:10 */}                                                        /* e:10 */
/* b:11 */                                                         /* e:11 */
/* b:12 */component/*i:12*/stress_comment                          /* e:12 */
/* b:13 */{                                                        /* e:13 */
/* b:14 */  provides/*i:14*/ihello/*i:14.1*/h ;                    /* e:14 */
/* b:15 */  requires/*i:15*/iworld/*i:15.2*/w ;                    /* e:15 */
/* b:16 */                                                         /* e:16 */
/* b:17 */  behavior                                               /* e:17 */
/* b:18 */  {                                                      /* e:18 */
/* b:19 */    on/*i:19*/h.hello/*i:19.0*/(/*i:19:1*/)/*i:19.2*/:   /* e:19 */
/* b:20 */        w.world/*i:20*/(/*i:20.1*/)/*i:20.2*/;           /* e:20 */
/* b:21 */  }                                                      /* e:21 */
/* b:22 */}                                                        /* e:22 */
/* b:23 */                                                         /* e:23 */
/* b:24 */interface/*i:24*/iworld                                  /* e:24 */
/* b:25 */{                                                        /* e:25 */
/* b:26 */  in/*i:26*/void/*i:26.1*/world/*i:26.2*/(/*i:26.3*/)/*i:26.4*/; /* e:26 */
/* b:27 */                                                         /* e:27 */
/* b:28 */  behavior                                               /* e:28 */
/* b:29 */  {                                                      /* e:29 */
/* b:30 */    on/*i:30*/world/*i:30.1*/: {/*i:30.2*/}              /* e:30 */
/* b:31 */  }                                                      /* e:31 */
/* b:32 */}                                                        /* e:32 */
/* b:33 */enum/*i:33*/E/*i:33.1*/{/*i:33.3*/                       /* e:33 */
/* b:34 */A/*i:34*/,/*i:34.1*/                                     /* e:34 */
/* b:35 */B/*i:35*/,/*i:35.1*/                                     /* e:35 */
/* b:36 */}/*i:36*/;/*i:36.1*/                                     /* e:36 */
// b:37 end
