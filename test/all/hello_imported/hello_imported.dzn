#dir "."
#file "hello_imported.dzn"
// Dezyne --- Dezyne command line tools
//
// Copyright © 2018, 2019 Rob Wieringa <rma.wieringa@gmail.com>
// Copyright © 2021 Paul Hoogendijk <paul@dezyne.org>
// Copyright © 2020, 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

//import interfaces.dzn;
//import components.dzn;

component hello_imported
{
  provides ihello h;
  requires iworld w;

  behavior
  {
    on h.hello(): w.world();
  }
}
#imported "interfaces.dzn"
//import iworld.dzn;

interface ihello
{
  in void hello();

  behavior
  {
    on hello: {}
  }
}
#imported "iworld.dzn"
interface iworld
{
  in void world();

  behavior
  {
    on world: {}
  }
}
#imported "components.dzn"
//import interfaces.dzn;

// This originally tested removing behavior content from imported
// models (this feature has been removed), and check compatible
// behavior when using the pre-processor.
/*
component comp {
  provides ihello h;

  behavior
  {
    on h.hello(): {
      //f(); // this error will not be detected!
    }
  }
}
*/
