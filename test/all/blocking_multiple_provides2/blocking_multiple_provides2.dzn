// Dezyne --- Dezyne command line tools
//
// Copyright © 2021 Paul Hoogendijk <paul@dezyne.org>
// Copyright © 2022, 2023 Rutger (regtur) van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  in void hello();

  behavior
  {
    on hello: {}
  }
}

interface iworld
{
  in void hello();
  out void world();

  behavior
  {
    on hello: world;
  }
}

component double_hello_block
{
  provides blocking ihello h_left;
  provides blocking ihello h_right;
  requires external iworld w_left;
  requires external iworld w_right;

  behavior
  {
    blocking on h_left.hello(): w_left.hello();
    blocking on h_right.hello(): w_right.hello();

    on w_left.world(): h_left.reply();
    on w_right.world(): h_right.reply();
  }
}

component blocking_multiple_provides2
{
  provides blocking ihello h_left;
  provides blocking ihello h_right;
  requires external iworld w_left;
  requires external iworld w_right;

  system
  {
    h_left <=> dhb.h_left;
    h_right <=> dhb.h_right;
    double_hello_block dhb;
    dhb.w_left <=> w_left;
    dhb.w_right <=> w_right;
  }
}
