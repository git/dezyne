// Dezyne --- Dezyne command line tools
//
// Copyright © 2018, 2020, 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// The queuefull_component0 triggers a queue-full error for the default
// queue-size of 3.
//
// Code:

interface ihello
{
  in void hello ();
  out void world ();
  behavior
  {
    on hello: {world;world;world;world;}
  }
}

component queuefull_component0
{
  provides ihello h;
  requires ihello w;
  behavior
  {
    on h.hello (): w.hello ();
    on w.world (): h.world ();
  }
}
