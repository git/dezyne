// Dezyne --- Dezyne command line tools
//
// Copyright © 2018 Rutger van Beusekom <rutger@dezyne.org>
// Copyright © 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  in bool hello ();
  in bool hi ();

  behavior
  {
    on hello: reply (false);
    on hi: reply (false);
  }
}

component hello_local_assign
{
  provides ihello h;
  requires iworld w;

  behavior
  {
    bool b = false;
    [!b] on h.hello ():
      {
        {
          bool b = true;
          {
            b = w.world ();
          }
          reply (b);
        }
      }
    [!b] on h.hi ():
      {
        bool b = w.world ();
        reply (b);
      }
  }
}

interface iworld
{
  in bool world ();

  behavior
  {
    on world: reply (false);
  }
}
