// Dezyne --- Dezyne command line tools
//
// Copyright © 2022, 2023 Rutger (regtur) van Beusekom <rutger@dezyne.org>
// Copyright © 2022 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
interface ihello
{
  in void hello();

  behavior
  {
    on hello:{}
  }
}

interface iworld
{
  in void hello();
  out void world();

  behavior
  {
    bool idle = true;
    [idle] on hello: idle = false;
    [!idle] on inevitable: {idle = true; world;}
  }
}

component blocking_top
{
  provides blocking ihello h;
  requires iworld w;

  requires blocking ihello p;

  behavior
  {
    blocking on h.hello(): {w.hello(); p.hello(); p.hello();}
    on w.world(): h.reply();
  }
}

component blocking_middle
{
  provides blocking ihello h;
  requires iworld w;

  requires blocking ihello p;

  behavior
  {
    bool first = true;
    [first] blocking on h.hello(): {w.hello(); first = false;}
    on w.world(): h.reply();

    [!first] on h.hello(): {p.hello(); first = true;}
  }
}

component blocking_bottom
{
  provides blocking ihello h;
  requires iworld w;

  behavior
  {
    blocking on h.hello(): w.hello();
    on w.world(): h.reply();
  }
}

component adapter
{
  provides iworld h;
  requires external iworld w;
  behavior
  {
    on h.hello (): w.hello ();
    on w.world (): h.world ();
  }
}

component external_adapter
{
  provides iworld h;
  requires external iworld w;
  system
  {
    adapter adapt;
    h <=> adapt.h;
    adapt.w <=> w;
  }
}

component collateral_blocking_bridges
{
  provides blocking ihello h;
  requires external iworld top_w;
  requires external iworld middle_w;
  requires external iworld bottom_w;
  system
  {
    blocking_top top;
    blocking_middle middle;
    blocking_bottom bottom;

    h <=> top.h;
    external_adapter top_adapter;
    top.w <=> top_adapter.h;
    top_adapter.w <=> top_w;

    top.p <=> middle.h;
    external_adapter middle_adapter;
    middle.w <=> middle_adapter.h;
    middle_adapter.w <=> middle_w;

    middle.p <=> bottom.h;
    external_adapter bottom_adapter;
    bottom.w <=> bottom_adapter.h;
    bottom_adapter.w <=> bottom_w;
  }
}
