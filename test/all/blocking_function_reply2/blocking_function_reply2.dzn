// Dezyne --- Dezyne command line tools
//
// Copyright © 2018, 2022 Rutger van Beusekom <rutger@dezyne.org>
// Copyright © 2019, 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  in void hello();
  in bool wait();
  in bool bye();
  out void world();

  behavior
  {
    bool idle = true;
    [idle] on hello: idle = false;
    [!idle] {
      on wait: {idle = true; reply(idle);}
      on bye: reply(idle);
      on inevitable: {idle = true; world;}
    }
  }
}

component blocking_function_reply2
{
  provides blocking ihello p;
  requires ihello r;

  behavior
  {
    bool idle = true;
    bool wait = false;
    [idle] on p.hello(): {idle = false; r.hello();}
    [!idle && !wait] {
      on p.bye(): f();
      blocking on p.wait(): wait = true;
      on r.world(): {idle = true; p.world();}
    }
    [!idle && wait] {
      on r.world(): {idle = true; wait = false; p.reply(true);}
    }
    void f() {reply(idle);}
  }
}
