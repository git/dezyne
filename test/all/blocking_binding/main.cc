// Dezyne --- Dezyne command line tools
//
// Copyright © 2019, 2020, 2022, 2023 Rutger (regtur) van Beusekom <rutger@dezyne.org>
// Copyright © 2022, 2023 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

#include "blocking_binding.hh"

#include <thread>

#include <dzn/locator.hh>
#include <dzn/runtime.hh>
#include <dzn/pump.hh>

std::string
read ()
{
  std::string str;
  {
    std::string line;
    while (std::cin >> line)
      str += (str.empty () ? "" : "\n") + line;
  }
  return str;
}

static bool
getopt (int argc, char const *argv[], std::string option)
{
  return argv + argc != std::find_if (argv + 1, argv + argc, [&option] (char const * s)
  {
    return s == option;
  });
}

int
main (int argc, char const *argv[])
{
  if (getopt (argc, argv, "--debug"))
    dzn::debug.rdbuf (std::clog.rdbuf ());

  dzn::locator locator;
  dzn::runtime runtime;
  locator.set (runtime);
  blocking_binding sut (locator);
  dzn::pump pump;
  locator.set (pump);

  sut.w.in.hello = [&] {};

  dzn::check_bindings (sut);

  std::string trace = read ();
  if (0);
  // trace
  else if (trace == "h.hello\nw.hello\nw.return\nw.world\nh.return")
    {
      int v = 0;
      pump ([&]
      {
        sut.h.in.hello (v);
        assert (v == 456);
      });
      pump ([&] {sut.w.out.world ();});
    }
  else
    {
      std::clog << "missing trace" << std::endl;
      return 1;
    }

  pump.wait ();

  return 0;
}
