// Dezyne --- Dezyne command line tools
//
// Copyright © 2021 Paul Hoogendijk <paul@dezyne.org>
// Copyright © 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
// Copyright © 2021, 2022, 2023 Rutger (regtur) van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  in bool hello ();
  in bool cruel ();

  behavior {
    on hello: reply(true);
    on hello: reply(false);
    on cruel: reply(true);
    on cruel: reply(false);
  }
}

interface iworld
{
  in void hello ();
  out void world ();

  in void cruel ();
  out void bye ();

  behavior {
    on hello: world;
    on cruel: bye;
  }
}

extern Exception $int$;

component Blocked
{
  provides blocking ihello h;
  requires blocking iworld w;

  behavior
  {
    bool waiting = false;

    blocking on h.hello (): {
      w.hello ();
      waiting = true;
    }

    [!waiting] on h.cruel(): h.reply(false); // So any cruel() call should return false

    on w.world (): {
      waiting = false;
      h.reply(true);
    }
  }
}

component Bottom
{
  provides blocking iworld p;
  requires external blocking iworld r;

  behavior
  {
    on p.hello(): blocking r.hello();
    on r.world(): {p.world(); p.reply ();}
    on p.cruel(): p.bye();
  }
}

component collateral_blocking_shell2
{
  provides blocking ihello h;
  requires external blocking iworld w;

  system
  {
    Blocked blocked;
    Bottom bottom;

    h <=> blocked.h;
    blocked.w <=> bottom.p;
    bottom.r <=> w;
  }
}
