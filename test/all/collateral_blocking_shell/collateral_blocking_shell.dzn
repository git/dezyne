// Dezyne --- Dezyne command line tools
//
// Copyright © 2021 Paul Hoogendijk <paul@dezyne.org>
// Copyright © 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
// Copyright © 2022, 2023 Rutger (regtur) van Beusekom <rutger@dezyne.org>
//
// This file is part of Dezyne.
//
// Dezyne is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Dezyne is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
//
// Commentary:
//
// Code:

interface ihello
{
  in void hello ();
  in void cruel ();

  behavior {
    on hello: {}
    on cruel: {}
  }
}

interface iworld
{
  in void hello ();
  out void world ();

  behavior {
    bool idle = true;
    on hello: idle = !idle;
    [!idle] on inevitable: {world; idle = true;}
  }
}

component Top
{
  provides blocking ihello h;
  requires blocking ihello w;

  behavior
  {
    on h.hello (): w.hello ();
    on h.cruel (): w.cruel ();
  }
}

component Blocked
{
  provides blocking ihello h;
  requires external iworld w0;
  requires external iworld w1;

  behavior
  {
    bool idle = true;
    bool idle_w0 = true;
    bool idle_w1 = true;
    [idle] blocking on h.hello (): {w0.hello (); w1.hello (); idle = false; idle_w0 = false; idle_w1 = false;}
    on h.cruel(): {}
    on w0.world (): { idle_w0 = true; if (idle_w1) { h.reply(); idle = true; } }
    on w1.world (): { idle_w1 = true; if (idle_w0) { h.reply(); idle = true; } }
  }
}

component collateral_blocking_shell
{
  provides blocking ihello h;
  requires external iworld w0;
  requires external iworld w1;

  system
  {
    Top top;
    Blocked blocked;

    h <=> top.h;
    top.w <=> blocked.h;
    blocked.w0 <=> w0;
    blocked.w1 <=> w1;
  }
}
