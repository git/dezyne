;;; Dezyne --- Dezyne command line tools
;;;
;;; Copyright © 2019, 2020, 2021, 2022, 2023 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;; Copyright © 2020, 2021, 2022, 2023 Rutger van Beusekom <rutger@dezyne.org>
;;; Copyright © 2021 Paul Hoogendijk <paul@dezyne.org>
;;;
;;; This file is part of Dezyne.
;;;
;;; Dezyne is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Dezyne is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.

(define-module (dzn vm run)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)

  #:use-module (ice-9 hcons)
  #:use-module (ice-9 match)

  #:use-module (dzn ast)
  #:use-module (dzn ast goops)
  #:use-module (dzn misc)
  #:use-module (dzn vm compliance)
  #:use-module (dzn vm goops)
  #:use-module (dzn vm report)
  #:use-module (dzn vm runtime)
  #:use-module (dzn vm step)
  #:use-module (dzn vm util)
  #:export (%exploring?
            extend-trace
            filter-compliance-error
            filter-error
            filter-illegal+implicit-illegal
            filter-implicit-illegal
            filter-implicit-illegal-only
            filter-match-error
            flush-defer
            interactive?
            livelock?
            mark-livelock-error
            run-defer-event
            run-flush
            run-external
            run-external-q
            run-external-modeling
            run-requires
            run-requires-flush
            run-silent
            run-to-completion
            run-to-completion*
            run-to-completion*-context-switch))

;;; Commentary:
;;;
;;; ’run’ implements run-to-completion for the Dezyne vm: the level
;;; above 'step'.
;;;
;;; Code:

;; Are we running "explore"?
(define %exploring? (make-parameter #f))

;;;
;;; Trace utilities
;;;

(define (trace-valid? trace)
  ((compose not .status car) trace))

(define (filter-compliance-error traces)
  (let ((non-compliance rest
                        (partition
                         (disjoin (compose (is-status? <compliance-error>) car)
                                  (compose (is-status? <match-error>) car))
                         traces)))
    (if (pair? rest) rest
        non-compliance)))

(define (filter-error traces)
  (let ((error rest (partition
                     (compose (conjoin (is? <error>)
                                       (negate (is-status? <match-error>)))
                              car) traces)))
    (if (pair? rest) rest
        error)))

(define (filter-illegal traces)
  (let ((illegal rest (partition
                       (compose (is-status? <illegal-error>) car)
                       traces)))
    (if (pair? illegal) illegal
        rest)))

(define (filter-implicit-illegal traces)
  (let ((illegal rest (partition
                       (compose (is-status? <implicit-illegal-error>) car)
                       traces)))
    (if (pair? rest) rest
        illegal)))

(define (filter-implicit-illegal-only traces)
  (let ((illegal
         rest (partition
               (conjoin (compose (is-status? <implicit-illegal-error>) car)
                        (compose (match-lambda
                                   ((trigger "<illegal>") #t)
                                   (_ #f))
                                 trace->string-trail))
               traces)))
    (if (pair? rest) rest
        illegal)))

(define (filter-illegal+implicit-illegal traces)
  (let ((illegal rest (partition
                       (compose
                        (disjoin (is-status? <illegal-error>)
                                 (is-status? <implicit-illegal-error>))
                        car)
                       traces)))
    (if (pair? illegal) illegal
        rest)))

(define (filter-match-error traces)
  (let ((match-error rest (partition
                           (compose (is-status? <match-error>) car)
                           traces)))
    (if (pair? rest) rest
        match-error)))

(define (filter-postponed-match traces)
  (let* ((postponed-match rest (partition
                                (compose (is-status? <postponed-match>) car)
                                traces))
         (valid? (find trace-valid? rest)))
    (if (or valid? (null? postponed-match)) rest
        postponed-match)))

(define (non-deterministic? traces)
  "Return #t when TRACES are a nondeterministic set, i.e.: at least two
valid PCs that are executing an imperative statement."
  (let* ((pcs (map car traces))
         (pcs (filter (negate (is-status? <error>)) pcs))
         (pcs (filter (disjoin
                       (negate (is-status? <postponed-match>))
                       (conjoin
                        (compose (conjoin
                                  (negate (is? <action>))
                                  (negate (is? <reply>))
                                  (negate (is? <trigger-return>))
                                  (disjoin (negate (is? <variable>))
                                           (compose null?
                                                    (cute tree-collect
                                                          (is? <action>)
                                                          <>))))
                                 .statement)
                        (compose (cute ast:parent <> <component>) .ast .status)))
                      pcs)))
    (and
     (every (compose (cute is-a? <> <runtime:component>) .instance) pcs)
     (let ((declarative imperative
                        (partition (compose ast:declarative? .statement)
                                   pcs)))
       (> (length imperative) 1)))))

(define-method (block-on-port trace)
  "Return TRACE blocked on port when trace head is an <action> or a
<trigger-return> on a blocking requires port, unless already blocked on
this statement.  Otherwise, return #f.

Blocking and resetting the #:statement on TRACE makes RTC? true, which
creates an opening to accept new modeling events.  Continuation of the
<action> or <trigger-return> happens when revisiting this method with a
TRACE that has BLOCKED still set to this port."

  (define (modeling-traces pc instance)
    (let* ((port (.ast instance))
           (ipc (clone pc #:trigger #f #:previous #f #:instance
                       #f #:trail '() #:statement #f))
           (modeling (modeling-names (.type port)))
           (traces (parameterize ((%sut instance)
                                  (%liveness? 'port)
                                  (%exploring? #f)
                                  (%strict? #f))
                     (append-map (cut run-to-completion ipc <>) modeling)))
           (traces (filter (compose (negate (is-status? <error>)) car) traces)))
      traces))

  (define (requires-modeling-armed? pc)
    (let* ((instance (.instance pc))
           (requires-ports (filter
                            (conjoin runtime:boundary-port?
                                     ast:requires?
                                     (negate (cute eq? <> instance)))
                            (%instances)))
           (modeling (filter (compose pair? modeling-names .type .ast)
                             requires-ports))
           (ports (filter (compose pair?
                                   (cute modeling-traces pc <>))
                          modeling)))
      (pair? ports)))

  (let* ((pc (car trace))
         (instance (.instance pc))
         (statement (.statement pc))
         (blocked (.blocked pc)))
    (and
     (not (is-a? (%sut) <runtime:port>))
     (is-a? instance <runtime:port>)
     (ast:requires? instance)
     (ast:blocking? instance)
     (not (ast:modeling? (.trigger pc)))
     (or (is-a? statement <action>)
         (is-a? statement <trigger-return>))
     (let ((blocked-port (and (pair? blocked) (caar blocked)))
           (blocked-pc (and (pair? blocked) (cdar blocked))))
       (and
        (not (ast:eq? statement (and=> blocked-pc .statement)))
        (or (and (is-a? (%sut) <runtime:system>)
                 (> (length (ast:provides-port* (runtime:%sut-model))) 1))
            (requires-modeling-armed? pc))
        (let* ((trace (cdr trace))
               (pc (blocked-on-boundary-reset pc))
               (pc (block pc instance))
               (trace (cons pc trace)))
          trace))))))

(define (mark-determinism-error trace)
  "Truncate TRACE up to including the component <initial-compound> and
mark it with <determinism-error>."
  (let* ((index (list-index (conjoin (compose (is? <runtime:component>) .instance)
                                     (compose ast:imperative? .statement))
                            trace))
         (pc (list-ref trace index))
         (trace (drop trace index))
         (error (make <determinism-error>
                  #:ast (.statement pc)
                  #:message "non-deterministic"))
         (pc (clone pc #:status error)))
    (cons pc trace)))

(define (mark-livelock-error trace index)
  (let* ((model (runtime:%sut-model))
         (ast-model (if (is-a? model <system>) model
                        (.behavior model)))
         (pc-loop (list-ref trace index))
         (ast-loop (or (.statement pc-loop) ast-model))
         (loop (make <livelock-error> #:ast ast-loop #:message "loop"))
         (pc-loop-error (clone pc-loop #:status loop))
         (trace-prefix trace-suffix (split-at trace index))
         (shorter-index (and=> (list-index
                                (cute pc-equal? <> pc-loop)
                                (reverse trace-prefix))
                               (cute - (length trace-prefix) <> 1)))
         (shorter-prefix (if (not shorter-index) trace-suffix
                             (drop trace-prefix shorter-index)))
         (pc (car shorter-prefix))
         (ast (or (.statement pc) ast-model))
         (error (make <livelock-error> #:ast ast #:message "livelock"))
         (pc (clone pc #:status error)))
    `(,pc
      ,@shorter-prefix
      ,pc-loop-error
      ,@trace-suffix)))

(define %livelock-threshold (make-parameter 42))
(define (livelock? trace)
  "Return the index of the start of the livelock loop, or false.  Use
%LIVELOCK-THRESHOLD for heuristics to avoid re-evaluating the same
prefix."
  (define* (trace-head-recurrence? trace)
    (let ((trace (filter
                  (conjoin
                   (disjoin (compose (cute eq? <> (%sut)) .instance)
                            (compose (is? <runtime:component>) .instance))
                   (negate (compose (is? <initial-compound>) .statement))
                   (negate (compose (is? <end-of-on>) .statement)))
                  trace)))
      (and (pair? trace)
           (find (cute pc-equal?
                       (car trace) <>) (reverse (cdr trace))))))

  (and (>= (length trace) (%livelock-threshold))
       (let* ((suffixes (reverse (unfold null? identity cdr trace)))
              (loop (find trace-head-recurrence? suffixes)))
         (match loop
           (#f
            (%livelock-threshold (* 2 (%livelock-threshold)))
            #f)
           ((pc tail ...)
            (%debug (current-source-location) "  ~s ~s <livelock>"
                    ((compose name .instance) pc)
                    (and=> (.trigger pc) trigger->string))
            (let ((index (and=> (list-index (cute pc-equal? <> pc)
                                            (reverse trace))
                                (cute - (length trace) <> 1))))
              index))))))

(define (interactive?)
  (isatty? (current-input-port)))

(define-method (extend-trace (trace <list>))
  "Return a list of traces, produced by appending TRACE to each of the
program-counters produced by taking a step."

  (define* ((mark-pc input orig-pc) pc)
    (let ((statement (.statement orig-pc)))
      (%debug (current-source-location) "match fail, ast ~s ~a, input ~s" (name statement)
              (and=> (trace->trail orig-pc) cdr)
              input)
      (cond ((.status pc)
             pc)
            ((or (and (%strict?) input)
                 (and input
                      (is-a? statement <trigger-return>)
                      (blocked-on-boundary? orig-pc)))
             (let ((error (make <match-error> #:ast statement #:input input
                                #:message "match")))
               (clone pc #:status error)))
            (else
             (clone pc #:status (make <postponed-match> #:ast statement
                                      #:input input))))))

  (define (blocked-collaterally? orig-pc pc)
    (match (.collateral pc)
      (((port . pc) t ...)
       (eq? pc orig-pc))
      (_ #f)))

  (let loop ((trace trace))
    (let ((pc (car trace)))
      (%debug pc)
      (cond ((.status pc)
             (list trace))
            ((livelock? trace)
             =>
             (compose list (cute mark-livelock-error trace <>)))
            ((rtc? pc)
             (list trace))
            ((block-on-port trace)
             => list)
            (else
             (let* ((o (.statement pc))
                    (instance (.instance pc))
                    (update-state?
                     (and (is-a? instance <runtime:component>)
                          (not (is-a? (%sut) <runtime:port>))
                          (is-a? o <action>)
                          (ast:out? o)))
                    (pc (if (not update-state?) pc
                            (let ((trigger (instance-rtc-trigger pc))
                                  (port (.port o)))
                              (update-shared-state pc instance port trigger
                                                   trace))))
                    (observable? (or (is-a? o <action>)
                                     (is-a? o <q-out>)
                                     (is-a? o <trigger-return>)))
                    (observable (and observable? (and=> (trace->trail pc) cdr)))
                    (pcs (step pc o))
                    (trace (if (any (disjoin
                                     (conjoin (is-status? <second-reply-error>)
                                              (const (is-a? (.statement pc)
                                                            <trigger-return>)))
                                     (cute blocked-collaterally? pc <>)) pcs)
                               (cdr trace)
                               trace))
                    (input pc (if (and observable (not (interactive?)))
                                  ((%next-input) pc)
                                  (values #f pc)))
                    (pcs (cond ((%exploring?)
                                pcs)
                               ((not observable)
                                pcs)
                               ((equal? input observable)
                                (map (cute clone <> #:trail (.trail pc)) pcs))
                               (else
                                (map (mark-pc input pc) pcs))))
                    (internal-compliance?
                     (and (is-a? (%sut) <runtime:system>)
                          (not (is-a? (%sut) <runtime:port>))
                          (or (and
                               (is-a? o <trigger-return>)
                               (and=> (.port o) ast:provides?)
                               (and=> (.trigger pc) .port)
                               (ast:provides?
                                (runtime:port instance (.port o))))
                              (and (is-a? o <flush-return>)
                                   (.trigger o)))))
                    (traces (map (cut cons <> trace) pcs))
                    (traces (if (not internal-compliance?) traces
                                (internal-check-compliance pc trace traces)))
                    (update? (and
                              (not internal-compliance?)
                              (is-a? (%sut) <runtime:system>)
                              (is-a? o <trigger-return>)
                              (runtime:boundary-port? instance)))
                    (traces (if (not update?) traces
                                (map
                                 (cute rewrite-trace-head
                                       (cut update-other-state <> instance
                                            #:direction? ast:requires?)
                                       <>)
                                 traces))))
               (cond
                ((ast:declarative? o)
                 (let ((declarative
                        imperative (partition
                                    (compose ast:declarative? .statement car)
                                    traces)))
                   (cond
                    ((pair? declarative)
                     (append imperative (append-map loop declarative)))
                    (else
                     traces))))
                (observable
                 traces)
                (else
                 (append-map loop traces)))))))))


;;;
;;; Run
;;;

;;; ’run’ loops over ’step’ until ’rtc?’ (run to completion done) and
;;; collects a trace (of program counters).

(define-method (run-to-completion-unmemoized (pc <program-counter>))
  "Return a list of traces produced by taking steps, starting from
PC until RTC?."

  (define (postponed-match? traces)
    (and (not (find trace-valid? traces))
         (let* ((traces (filter (compose (is-status? <postponed-match>) car) traces))
                (pcs (map car traces)))
           (and (pair? traces)
                traces))))

  (define (observable pc)
    (and=> (trace->trail pc) cdr))

  (define (reset-posponed-match traces)
    (define (reset-posponed-match trace)
      (let* ((pc (car trace))
             (observe-pc (cadr trace))
             (trail (.trail pc))
             (input (.input (.status pc)))
             (drop-event? (and (pair? trail) (equal? input (observable observe-pc))))
             (trail (if drop-event? (cdr trail) trail))
             (trace (rewrite-trace-head (cut clone <> #:status #f #:trail trail) trace)))
        trace))
    (let* ((traces (map reset-posponed-match traces))
           (traces (delete-duplicates traces trace-equal?)))
      (loop (append-map extend-trace traces))))

  (define (choice-label trace)
    (match (trace->trail trace)
      ((label ... ((and ($ <trigger-return>) return) . string) (#f . "<postponed-match>"))
       (.event.name return))
      ((label ... (choice . string) (#f . "<postponed-match>")) choice)))

  (define (choice-labels traces)
    (let ((labels (map choice-label traces)))
      (delete-duplicates labels ast:equal?)))

  (define (choose-postponed-match traces)
    (define (label trace)
      (let* ((label (label->string (choice-label trace)))
             (pc (cadr trace))
             (instance (.instance pc))
             (port-name (string-join (runtime:instance->path instance) "."))
             (prefix (if (is-a? (%sut) <runtime:port>) ""
                         (string-append port-name "."))))
        (format #f "~a~a" prefix label)))
    (show-eligible (delete-duplicates (map label traces)) #:traces traces)
    (let* ((input pc ((%next-input) pc))
           (traces (map cdr traces))    ;drop <postponed-match> pc
           (traces (filter (compose (cute equal? input <>) observable car) traces)))
      (loop (append-map extend-trace traces))))

  (define (interface-non-deterministic? traces)
    (let loop ((traces traces))
      (match traces
        (((pc0 tail0 ...) (pcs tails ...) ...)
         (define (state-equal? pc0 pc1)
           (let ((variables0 (get-variables pc0 (%sut)))
                 (variables1 (get-variables pc1(%sut))))
             (ast:equal? variables0 variables1)))
         (let ((trail0 (trace->string-trail tail0))
               (trails (filter-map (lambda (pc tail)
                                     (and (not (state-equal? pc0 pc))
                                          (trace->string-trail tail)))
                                   pcs tails)))
           (or (member trail0 trails)
               (loop (map cons pcs tails)))))
        (() #f))))

  (define (interface-postponed-non-deterministic? traces)
    (let ((traces (filter (compose (is? <action>) .ast .status car) traces)))
      (interface-non-deterministic? traces)))

  (define (interface-mark-determinism-error trace)
    (match trace
      ((pc-postponed pc trace ...)
       (let* ((error (make <determinism-error>
                       #:ast (.statement pc)
                       #:message "non-deterministic"))
              (pc (clone pc #:status error)))
         (cons pc trace)))))

  (define (mark-end-of-trail traces)
    (define (set-end-of-trail labels pc)
      (let* ((statement (.statement pc))
             (labels (make <labels> #:elements labels))
             (status (make <end-of-trail> #:ast statement #:labels labels))
             (pc (clone pc #:statement #f)))
        (clone pc #:status status)))
    (let* ((labels (choice-labels traces))
           (traces (map cdr traces)))   ;drop <postponed-match> pc
      (map (cute rewrite-trace-head (cute set-end-of-trail labels <>) <>)
           traces)))

  (define (previous-provides? trace)
    (let ((previous (any .trigger trace)))
      (and previous
           (and=> (.port previous) ast:provides?))))

  (define (must-switch? trace)
    (and (not (is-a? (%sut) <runtime:port>))
         (not (previous-provides? trace))))

  (define (loop traces)
    (let* ((traces (if (%exploring?) traces (filter-illegal traces)))
           (traces (filter-match-error traces))
           (traces (filter-postponed-match traces))
           (traces (filter-implicit-illegal traces))
           (pcs (map car traces)))
      (cond
       ((null? traces)
        '())
       ((and (%exploring?)
             (find (compose (is-status? <livelock-error>) car) traces))
        traces)
       ((every (conjoin (negate (is-status? <postponed-match>))
                        (disjoin rtc? (is-status? <match-error>)))
               pcs)
        (let* ((to-switch traces (partition must-switch? traces))
               (traces (append traces (map switch-context to-switch)))
               (done todo (partition rtc? traces)))
          (append done
                  (loop (append-map extend-trace todo)))))
       ((postponed-match? traces)
        =>
        (lambda (traces)
          (cond
           ((and (not (%exploring?))
                 (non-deterministic? traces))
            (let ((traces (filter-implicit-illegal traces)))
              (map mark-determinism-error traces)))
           ((or (= (length traces) 1)
                (= (length (choice-labels traces)) 1))
            (reset-posponed-match traces))
           ((and (is-a? (%sut) <runtime:port>)
                 (%strict?)
                 (interface-postponed-non-deterministic? traces))
            (map interface-mark-determinism-error traces))
           ((and (not (%exploring?)) (interactive?))
            (choose-postponed-match traces))
           (else
            (mark-end-of-trail traces)))))
       ((and (is-a? (%sut) <runtime:port>)
             (not (%exploring?))
             (%strict?)
             (interface-non-deterministic? traces))
        (map interface-mark-determinism-error traces))
       (else
        (loop (append-map extend-trace traces))))))

  (let ((traces (extend-trace (list pc))))
    (cond
     ((and (not (%exploring?))
           (non-deterministic? traces))
      (let ((traces (filter-implicit-illegal traces)))
        (map mark-determinism-error traces)))
     (else
      (loop traces)))))

(define-method (run-to-completion-unmemoized (pc <program-counter>) event)
  "Return a list of traces produced by taking steps, starting from PC
with EVENT as first step, until RTC?."
  (let ((pc (if (eq? event 'rtc) pc
                (begin-step pc event))))
    (run-to-completion-unmemoized pc)))

(define run-to-completion
  (let ((cache (make-weak-key-hash-table 523))
        (gc-buffer (make-gc-buffer 256)))
    (lambda (pc event)
      "Memoizing version of RUN-TO-COMPLETION-UNMEMOIZED."
      (if (not (%exploring?)) (run-to-completion-unmemoized pc event)
          (let* ((event-string (cond ((string? event) event)
                                     ((eq? event 'rtc) "*rtc*")
                                     (else (trigger->string event))))
                 (sut (%sut))
                 (root (ast:parent (.ast sut) <root>))
                 (key (parameterize ((%sut #f))
                        (string-append
                         (number->string (.id root))
                         (runtime:dotted-name sut)
                         (pc->string pc)
                         event-string))))
            (gc-buffer key)
            (or (hash-ref cache key)
                (let ((result (run-to-completion-unmemoized pc event)))
                  (hash-set! cache key result)
                  result)))))))

(define-generic run-to-completion)

(define-method (extend-trace (trace <list>) producer)
  "Return a list of traces produced running PRODUCER or the PC (head
of) TRACE, extending TRACE."
  (let* ((pc (car trace))
         (traces (producer pc)))
    (map (cute append <> trace) traces)))

(define-method (run-to-completion (trace <list>) event)
  "Return a list of traces produced by RUN-TO-COMPLETION, extending TRACE."
  (extend-trace trace (cute run-to-completion <> event)))

(define-method (run-flush (pc <program-counter>))
  "Return a list of traces produced by taking steps, starting by flushing (%SUT),
until RTC?."
  (if (.status pc) '()
      (let ((pc (flush pc)))
        (run-to-completion-unmemoized pc))))

(define-method (run-flush (trace <list>))
  "Return a list of traces produced by RUN-FLUSH, extending TRACE."
  (extend-trace trace run-flush))

(define-method (run-flush (trace <list>) (instance <runtime:component>))
  (extend-trace trace (cute run-flush <> instance)))

(define-method (run-flush (pc <program-counter>))
  "Return a list of traces produced by taking steps, starting by flushing (%SUT),
until RTC?."
  (let ((pc (flush pc)))
    (run-to-completion-unmemoized pc)))

(define-method (run-flush (pc <program-counter>) (instance <runtime:component>))
  (let ((pc (clone pc #:instance instance)))
    (run-flush pc)))

(define-method (run-silent (pc <program-counter>) (port <runtime:port>))
  (define (update-state pc port-pc)
    (set-state pc (get-state port-pc port)))
  (%debug (current-source-location) "run-silent... ~s" (name port))
  (let ((modeling-names (modeling-names port)))
    (if (null? modeling-names) '()
        (let* ((ipc (clone pc #:trigger #f #:previous #f #:instance #f #:trail '() #:statement #f))
               (traces (parameterize ((%sut port)
                                      (%exploring? #t)
                                      (%strict? #t))
                         (append-map (cute run-to-completion ipc <>) modeling-names)))
               (traces (filter (conjoin (compose null? trace->trail)
                                        (compose (negate .status) car))
                               traces)))
          (map (cute rewrite-trace-head (cute update-state pc <>) <>) traces)))))

(define-method (run-silent (pc <program-counter>) event)
  (let* ((component ((compose .type .ast) (%sut)))
         (trigger (clone (string->trigger event) #:parent component))
         (port-name (.port.name trigger))
         (port-instance (runtime:port-name->instance port-name))
         (traces (run-silent pc port-instance)))
    (map car traces)))

(define-method (run-silent traces)
  (let* ((pcs (map last traces))
         (pcs (map (cute clone <> #:status #f) pcs)))
    (append-map (cute run-silent <> (%sut)) pcs)))

(define-method (run-external-modeling (pc <program-counter>) (port <runtime:port>))
  (define (update-state pc port-pc)
    (let ((pc (set-state pc (get-state port-pc port))))
      (clone pc #:external-q (.external-q port-pc) #:status (.status port-pc))))
  (%debug (current-source-location) "run-external-modeling... ~s" (name port))
  (let* ((r:other-port (runtime:other-port port))
         (external? (and (ast:requires? r:other-port)
                         (ast:external? r:other-port))))
    (if (not external?) '()
        (let ((modeling-names (modeling-names port)))
          (if (null? modeling-names) '()
              (let* ((previous (.previous pc))
                     (ipc (clone pc #:trigger #f #:previous #f #:instance #f
                                 #:trail '() #:statement #f))
                     (traces (parameterize ((%sut port)
                                            (%liveness? 'component)
                                            (%exploring? #t)
                                            (%strict? #f))
                               (append-map (cute run-to-completion ipc <>) modeling-names)))
                     (traces (filter (compose
                                      (disjoin not (is? <queue-full-error>))
                                      .status car)
                                     traces)))
                (map (cute rewrite-trace-head (cute update-state pc <>) <>) traces)))))))

(define-method (run-external-modeling (pc <program-counter>))
  (let* ((ports (filter (conjoin runtime:boundary-port?
                                 ast:external? ast:requires?)
                        (%instances))))
    (append-map (cute run-external-modeling pc <>) ports)))

(define-method (run-external-modeling (pc <program-counter>) event)
  (define (event-executed? port-instance trace)
    (let ((status (.status (car trace)))
          (trail (map cdr (trace->trail trace))))
      (or (is-a? status <queue-full-error>)
          (let* ((pc (car trace))
                 (pc trigger (dequeue-external pc port-instance)))
            (and trigger
                 (let ((trigger (trigger->component-trigger port-instance
                                                            trigger)))
                   (equal? (trigger->string trigger) event)))))))
  (let* ((component ((compose .type .ast) (%sut)))
         (trigger (clone (string->trigger event) #:parent component))
         (port-name (.port.name trigger))
         (port-instance (runtime:port-name->instance port-name))
         (traces (run-external-modeling pc port-instance))
         (traces (filter (cute event-executed? port-instance <>) traces)))
    (map car traces)))

(define-method (run-interface (pc <program-counter>) (event <string>))
  (define (event-executed? trace)
    (let ((trail (trace->string-trail trace)))
      (and (pair? trail)
           (equal? event (car trail)))))
  (let* ((interface ((compose .type .ast %sut))))
    (cond
     ((in-event? event)
      (let* ((modeling-names (modeling-names interface))
             (silent-traces (if (null? modeling-names) '()
                                (run-silent pc (%sut))))
             (silent-pcs (map car silent-traces))
             (pcs (cons pc silent-pcs))
             (traces (map list pcs)))
        (append-map (cute run-to-completion <> event) (cons (list pc) traces))))
     (else
      (let* ((modeling? (member event '("inevitable" "optional")))
             (pc (if modeling? pc
                     (clone pc #:trail (cons event (.trail pc)))))
             (modeling-names (if modeling? (list event) (modeling-names)))
             (traces (append-map (cute run-to-completion pc <>) modeling-names))
             (traces (parameterize ((%modeling? modeling?))
                       (filter (disjoin
                                event-executed?
                                (compose (is-status? <determinism-error>) car))
                               traces))))
        traces)))))

(define-method (run-requires-flush (pc <program-counter>) event)
  (let* ((component ((compose .type .ast) (%sut)))
         (trigger (string->trigger event))
         (trigger (clone trigger #:parent component))
         (port-name (.port.name trigger))
         (port-instance (runtime:port-name->instance port-name))
         (component-port (runtime:other-port port-instance))
         (component-trigger (trigger->component-trigger component-port
                                                        trigger))
         (instance (.container component-port))
         (collateral-blocked? (or (blocked-on-boundary? pc)
                                  (and instance
                                       (get-handling pc instance)
                                       (blocked-port pc instance))))
         (traces (if collateral-blocked? (list (list pc))
                     (run-flush pc instance))))
    traces))

(define-method (run-requires-flush trace event)
  (extend-trace trace (cute run-requires-flush <> event)))

(define-method (run-requires (pc <program-counter>) event)
  (define (event-executed? port-instance trace)
    (let ((status (.status (car trace)))
          (trail (map cdr (trace->trail trace))))
      (or (is-a? status <queue-full-error>)
          (and (pair? trail)
               (equal? event (car trail)))
          (and (null? trail)
               (let* ((pc (car trace))
                      (pc trigger (dequeue-external pc port-instance)))
                 (and trigger
                      (equal? (trigger->string trigger) event)))))))
  (%debug (current-source-location) "run-requires... ~s" event)
  (let* ((component ((compose .type .ast) (%sut)))
         (trigger (string->trigger event))
         (trigger (clone trigger #:parent component))
         (port-name (.port.name trigger))
         (port-instance (runtime:port-name->instance port-name))
         (interface ((compose .type .ast) port-instance))
         (modeling-names (modeling-names interface))
         (trail (cons event (.trail pc)))
         (pc (clone pc #:trail trail #:status #f))
         (modeling-events (map (cute string-append port-name "." <>) modeling-names))
         (traces (append-map (cute run-to-completion pc <>) modeling-events))
         (traces (filter (cute event-executed? port-instance <>) traces))
         (traces (filter-match-error traces))
         (errors (filter (compose .status car) traces)))
    (if (pair? errors) errors
        (append-map (cute run-requires-flush <> event) traces))))

(define-method (run-defer-event (pc <program-counter>) event)
  (%debug (current-source-location) "run-defer-event ~a pc: ~s" event pc)
  (let* ((pc (prune-defer pc))
         (defer (.defer pc))
         (trail (.trail pc))
         (trail (if (equal? event "<defer>") trail
                    (cons event trail))))
    (define (livelock? trace)
      (let* ((new-pc (car trace))
             (pc (clone pc #:trail trail)))
        (and (requires-trigger? event)
             (rtc-program-counter-equal? new-pc pc)
             (>= (length (.defer new-pc)) (length  defer))
             (1- (length trace)))))
    (if (null? defer) '()
        (let* ((defer-pc (car defer))
               (statement (.statement defer-pc))
               (statement (.statement statement))
               (instance (.instance defer-pc))
               (defer-qout (make <defer-qout>
                             #:statement statement
                             #:location (.location statement)))
               (defer-qout (clone defer-qout #:parent (.parent statement)))
               (pc (clone pc
                          #:defer (cdr defer)
                          #:running-defer? instance
                          #:instance instance
                          #:statement defer-qout
                          #:trigger (.trigger defer-pc)
                          #:trail trail))
               (pc (graft-locals pc defer-pc))
               (traces (run-to-completion pc 'rtc))
               (blocked traces
                        (partition (compose blocked-on-boundary? car) traces))
               (collateral-blocked? (or (blocked-on-boundary? pc)
                                        (and instance
                                             (get-handling pc instance)
                                             (blocked-port pc instance))))
               (traces (if collateral-blocked? traces
                           (append-map (cute run-flush <> instance) traces)))
               (traces (map (cute rewrite-trace-head
                                  (cute clone <> #:running-defer? #f) <>)
                            traces))
               (traces (append blocked traces))
               (livelock traces (partition livelock? traces))
               (livelock (map (cute mark-livelock-error <> <>)
                              livelock
                              (map livelock? livelock))))
          (append livelock traces)))))

(define-method (flush-defer (pc <program-counter>))
  (let* ((event "<defer>")
         (traces (run-defer-event pc event))
         (blocked? (pair? (.blocked pc)))
         (traces (if (not blocked?) traces
                     (append-map
                      (cute run-to-completion*-context-switch <> event)
                      traces)))
         (traces flush (partition (compose null? .defer car) traces)))
    (append traces (append-map flush-defer flush))))

(define-method (flush-defer trace)
  (extend-trace trace flush-defer))

(define-method (run-external-q (pc <program-counter>) (instance <runtime:port>))
  (let* ((other-port (runtime:other-port instance))
         (other-instance (.container other-port)))
    (define (q-trigger q-trigger)
      (let* ((port-name (.name (.ast other-port)))
             (trigger (.trigger pc))
             (q-trigger (make <q-trigger>
                          #:event.name (.event.name q-trigger)
                          #:port.name port-name
                          #:location (.location q-trigger))))
        (clone q-trigger #:parent (.type (.ast other-instance)))))
    (let* ((pc trigger (dequeue-external pc instance))
           (q-in (make <q-in> #:trigger trigger))
           (q-in (clone q-in #:location (.location trigger)))
           (pc (push-pc pc other-instance q-in))
           (q-trigger (q-trigger trigger))
           (pc (enqueue pc trigger other-instance q-trigger))
           (q-in-pc pc)
           (pc (clone pc #:statement #f))
           (trace (list pc q-in-pc)))
      (list trace))))

(define-method (run-external (pc <program-counter>) event)
  (%debug (current-source-location) "run-external ~a pc: ~s" event pc)
  (let ((queues (.external-q pc)))
    (if (null? queues) '()
        (match (external-trigger-in-q? pc event)
          ((port q ...)
           (run-external-q pc (or port (%sut))))
          (_
           (let* ((model (runtime:%sut-model))
                  (ast (if (is-a? model <system>) model
                           (.behavior model)))
                  (error (make <match-error> #:ast ast #:input event
                               #:message "match"))
                  (pc (clone pc #:status error)))
             (%debug (current-source-location) "<match> ~a pc: ~s" event pc)
             (list (list pc) )))))))

(define-method (run-to-completion* (pc <program-counter>) event)
  (define (illegal-trace pc)
    (let ((illegal (make <implicit-illegal-error> #:ast (.trigger pc)
                         #:message "illegal")))
      (list (list (clone pc #:status illegal)))))
  (define (modeling? trace)
    (and=> (any .trigger (reverse trace)) ast:modeling?))
  (%debug (current-source-location) "run-to-completion*: ~a" event)
  (cond
   ((is-a? (%sut) <runtime:port>)
    (run-interface pc event))
   ((external-trigger-in-q? pc event)
    (let ((pc (clone pc #:trail (cons event (.trail pc)))))
      (run-external pc event)))
   ((and (external-trigger? event)
         (trigger-in-q? pc event))
    (run-requires-flush pc event))
   ((external-trigger? event)
    (let* ((pcs (cons pc (run-external-modeling pc event)))
           (traces (append-map (cute run-external <> event) pcs)))
      (append-map (cute run-requires-flush <> event) traces)))
   ((requires-trigger? event)
    (let* ((defer? (and (pair? (.defer pc))
                        (not (%strict?))))
           (defer-traces (if (not defer?) '()
                             (run-defer-event pc event))))
      (append
       defer-traces
       (let* ((pcs (cons pc (run-external-modeling pc event)))
              (port (.port (string->trigger event)))
              (blocked-port (and=> (blocked-on-boundary? pc event)
                                   .ast))
              (traces (append-map (cute run-requires <> event) pcs)))
         (if (or (not port) (not (ast:eq? port blocked-port))) traces
             (filter (negate modeling?) traces))))))
   ((provides-trigger? event)
    (if (blocked-on-boundary-provides? pc event) (illegal-trace pc)
        (run-to-completion pc event)))
   ((defer-event? pc event)
    (run-defer-event pc event))
   (else
    '())))

(define-method (run-to-completion* trace event)
  (extend-trace trace (cute run-to-completion* <> event)))

(define-method (defer-run-to-completion-rtc (pc <program-counter>))
  (let* ((instance (.running-defer? pc))
         (pc (clone pc #:running-defer? #f))
         (instance (if (is-a? instance <runtime:component>) instance
                       (.container (runtime:other-port instance))))
         (traces (run-to-completion pc 'rtc))
         (traces (map (cute rewrite-trace-head
                            (cute clone <> #:instance instance)
                            <>)
                      traces))
         (done traces
               (partition
                (disjoin
                 (compose blocked-on-boundary? car)
                 (compose (conjoin (cute get-handling <> instance)
                                   (cute blocked-port <> instance))
                          car)
                 (compose (is-status? <error>) car))
                traces)))
    (append done
            (append-map run-flush traces))))

(define-method (run-to-completion*-context-switch (pc <program-counter>) event)
  (%debug (current-source-location) "run-to-completion*-context-switch: ~a" event)
  (let* ((orig-pc pc)
         (blocked-on-action? (blocked-on-action? pc event))
         (pc (if (and (blocked-on-boundary? pc event)
                      (or blocked-on-action? (return-trigger? event)))
                 (blocked-on-boundary-switch-context pc event)
                 pc))
         (bob-switched? (not (eq? pc orig-pc)))
         (switched? (not (eq? pc orig-pc)))
         (trigger? (or (is-a? (%sut) <runtime:port>)
                       (provides-trigger? event)
                       (and (not blocked-on-action?)
                            (requires-trigger? event))))
         (pc (if (or switched? trigger?) pc
                 (blocked-on-boundary-collateral-release pc)))
         (pc (if (provides-trigger? event) pc
                 (switch-context pc)))
         (switched? (not (eq? pc orig-pc)))
         (pc (if switched? pc
                 (clone pc #:instance #f)))
         (skip-rtc? (or (not switched?)
                        (provides-trigger? event)))
         (pc (if (or trigger? (not switched?) (rtc-event? event)) pc
                 (clone pc #:trail (cons event (.trail pc)))))
         (traces (cond ((.running-defer? pc)
                        (defer-run-to-completion-rtc pc))
                       (skip-rtc?
                        (run-to-completion* pc event))
                       (else
                        (run-to-completion pc 'rtc)))))
    (if (or skip-rtc? (not trigger?)) traces
        (append-map (cute run-to-completion* <> event) traces))))

(define-method (run-to-completion*-context-switch trace event)
  (extend-trace trace (cute run-to-completion*-context-switch <> event)))
