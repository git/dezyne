;;; Dezyne --- Dezyne command line tools
;;;
;;; Copyright © 2019, 2020, 2022, 2023 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Dezyne.
;;;
;;; Dezyne is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Dezyne is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (dzn vm evaluate)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)

  #:use-module (ice-9 match)

  #:use-module (dzn ast goops)
  #:use-module (dzn ast)
  #:use-module (dzn misc)
  #:use-module (dzn vm goops)
  #:use-module (dzn vm runtime)

  #:export (eval-expression
            true?))

(define-method (expr:equal? (left <enum-literal>) (right <enum-literal>))
  (and (ast:node-eq? (.type left) (.type right))
       (equal? (.field left) (.field right))))

(define-method (expr:equal? (left <literal>) (right <literal>))
  (equal? (.value left) (.value right)))

(define-method (true? (o <literal>))
  (equal? (.value o) "true"))

(define-method (literal value)
  (match value
    (#t (make <literal> #:value "true"))
    (#f (make <literal> #:value "false"))
    ((? number?) (make <literal> #:value value))))

(define-method (eval-expression (pc <program-counter>) (o <expression>))
  (define get-state (@ (dzn vm util) get-state))
  (let ((state (if (.instance pc) (.variables (get-state pc)) '())))
    (match o
      (($ <literal>)
       o)
      (($ <action>)
       (ast:type o))
      (($ <shared-var>)
       (let ((port (runtime:other-port (runtime:port (.instance pc) (.port o)))))
         (eval-expression pc (assoc-ref (.variables (get-state pc port)) (.name o)))))
      ((and ($ <var>) (= .variable.name name))
       (eval-expression pc (assoc-ref state name)))
      ((and ($ <not>) (= .expression expression))
       (literal (not (true? (eval-expression pc expression)))))
      ((and ($ <equal>) (= .left left) (= .right right))
       (literal (expr:equal? (eval-expression pc left) (eval-expression pc right))))
      ((and ($ <not-equal>) (= .left left) (= .right right))
       (literal (not (expr:equal? (eval-expression pc left) (eval-expression pc right)))))
      ((and ($ <and>) (= .left left) (= .right right))
       (literal (and (true? (eval-expression pc left)) (true? (eval-expression pc right)))))
      ((and ($ <or>) (= .left left) (= .right right))
       (literal (or (true? (eval-expression pc left)) (true? (eval-expression pc right)))))
      ((and ($ <field-test>) (= .variable left) (= .field right))
       (literal (equal? (.field (eval-expression pc (make <var> #:name (.name left)))) right)))
      (($ <shared-field-test>)
       (let ((port (runtime:other-port (runtime:port (.instance pc) (.port o)))))
         (literal (equal? (.field (eval-expression pc (assoc-ref (.variables (get-state pc port)) (.variable.name o)))) (.field o)))))
      ((and ($ <plus>) (= .left left) (= .right right))
       (literal (+ (.value (eval-expression pc left)) (.value (eval-expression pc right)))))
      ((and ($ <minus>) (= .left left) (= .right right))
       (literal (- (.value (eval-expression pc left)) (.value (eval-expression pc right)))))
      ((and ($ <less>) (= .left left) (= .right right))
       (literal (< (.value (eval-expression pc left)) (.value (eval-expression pc right)))))
      ((and ($ <less-equal>) (= .left left) (= .right right))
       (literal (<= (.value (eval-expression pc left)) (.value (eval-expression pc right)))))
      ((and ($ <greater>) (= .left left) (= .right right))
       (literal (> (.value (eval-expression pc left)) (.value (eval-expression pc right)))))
      ((and ($ <greater-equal>) (= .left left) (= .right right))
       (literal (>= (.value (eval-expression pc left)) (.value (eval-expression pc right)))))
      ((and ($ <otherwise>) (= .value value))
       (eval-expression pc value))
      ((and ($ <group>)
            (= .expression expression)) (eval-expression pc expression))
      (_
       o))))
