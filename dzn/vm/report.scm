;;; Dezyne --- Dezyne command line tools
;;;
;;; Copyright © 2018, 2019, 2020, 2021, 2022, 2023 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;; Copyright © 2018, 2019 Rob Wieringa <rma.wieringa@gmail.com>
;;; Copyright © 2020, 2021, 2022, 2023 Rutger van Beusekom <rutger@dezyne.org>
;;;
;;; This file is part of Dezyne.
;;;
;;; Dezyne is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Dezyne is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (dzn vm report)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)

  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)

  #:use-module (dzn ast goops)
  #:use-module (dzn ast)
  #:use-module (dzn code language dzn)
  #:use-module (dzn misc)
  #:use-module (dzn trace)
  #:use-module (dzn vm ast)
  #:use-module (dzn vm goops)
  #:use-module (dzn vm runtime)
  #:use-module (dzn vm util)
  #:use-module (dzn ast wfc)
  #:export (%modeling?
            debug:lts->alist
            display-trace-n
            display-trails
            set-trigger-locations
            strip-sut-prefix
            trace-equal?
            trace-name
            trace->trail
            trace->component-trail
            trace->string-trail
            trace->component-string-trail
            report))

;;;
;;; Trail (a.k.a. events, named event trace)
;;;

;; Show modeling events on trail?
(define %modeling? (make-parameter #F))

(define (display-trails traces)
  (when (pair? traces)
    (display-trail-n (car traces) 0)))

(define (display-trail-n t i)
  (when (> i 0)
    (format (current-error-port) "\ntrace [~a]:\n" i))
  (display-trail t))

(define-method (display-trail (o <list>))
  (display (string-join (map cdr (trace->trail o)) "\n" 'suffix)))

(define-method (trace->trail (o <list>))
  (filter-map trace->trail (reverse o)))

(define-method (trace->trail (o <program-counter>))
  (and (pc-event? o) (pc->event o)))

(define (strip-sut-prefix o)
  (if (string-prefix? "sut." o) (substring o 4) o))

(define (trace->string-trail trace)
  (let ((trail (map cdr (trace->trail trace))))
    (map strip-sut-prefix trail)))

(define-method (trace->component-trail (o <list>))
  (filter-map trace->component-trail (reverse o)))

(define-method (trace->component-trail (o <program-counter>))
  (and (pc-arrow? o) (pc->component-event o)))

(define (trace->component-string-trail trace)
  (let ((trail (map cdr (trace->component-trail trace))))
    (map strip-sut-prefix trail)))

(define (trace-equal? a b)
  (let ((pc-a (car a))
        (pc-b (car b)))
    (and (rtc-program-counter-equal? pc-a pc-b)
         (ast:eq? (and=> (.previous pc-a) .statement)
                  (and=> (.previous pc-b) .statement))
         (equal? (trace->string-trail a)
                 (trace->string-trail b)))))

(define (debug:lts->alist pc->state-number lts)
  (define (entry->pair from pc+traces)
    (match pc+traces
      ((pc traces ...)
       (cons from
             (map (lambda (trace)
                    (append (trace->string-trail trace)
                            (list (pc->state-number (car trace)))))
                  traces)))))
  (let ((alist (hash-map->list entry->pair lts)))
    (sort alist (match-lambda* (((from-a tail-a ...) (from-b tail-b ...))
                                (< from-a from-b))))))

;;; events predicate

(define-method (pc-event? (o <program-counter>))
  (or (and=> (.status o) (negate (is? <end-of-trail>)))
      (pc-event? o (.statement o))))

(define-method (pc-event? (pc <program-counter>) (o <statement>))
  (pc-event? (.instance pc) o))

(define-method (pc-event? (pc <program-counter>) (o <defer-qout>))
  #t)

(define-method (pc-event? (pc <program-counter>) (o <initial-compound>))
  (pc-event? (.instance pc) (.trigger pc)))

(define-method (pc-event? (o <runtime:port>) (trigger <trigger>))
  (let ((modeling? (ast:modeling? trigger)))
    (or (and (eq? o (%sut))
             (not modeling?))
        (and (%modeling?)
             modeling?)
        (and (is-a? (.container o) <runtime:foreign>)
             (runtime:boundary-port? (runtime:other-port o))))))

(define-method (pc-event? (o <runtime:component>) (trigger <trigger>))
  (let ((port (.port trigger)))
    (and port
         (let ((r:other-port (runtime:other-port (runtime:port o port))))
           (runtime:boundary-port? r:other-port)))))

(define-method (pc-event? (o <runtime:component>) (trigger <q-trigger>))
  #f)

(define-method (pc-event? (o <runtime:port>) (q-in <q-in>))
  #t)

(define-method (pc-event? (o <runtime:component>) (q-in <q-in>))
  #t)

(define-method (pc-event? (o <runtime:port>) (action <action>))
  (or (ast:in? action)
      (is-a? (%sut) <runtime:port>)
      (and (ast:requires? o)
           (not (ast:external? o)))))

(define-method (pc-event? (o <runtime:component>) (action <action>))
  (let* ((port (.port action))
         (r:port (runtime:port o port))
         (r:other-port (runtime:other-port r:port)))
    (or (ast:injected? port)
        (runtime:boundary-port? r:other-port))))

(define-method (pc-event? (o <runtime:component>) (q-out <q-out>))
  (let* ((trigger (.trigger q-out))
         (port (.port trigger)))
    (ast:provides? port)))

(define-method (pc-event? (pc <program-counter>) (o <trigger-return>))
  (and (not (is-a? (.event (.trigger pc)) <modeling-event>))
       (pc-event? (.instance pc) o)))

(define-method (pc-event? (o <runtime:port>) (return <trigger-return>))
  (or (is-a? (%sut) <runtime:port>)
      (and (runtime:boundary-port? o)
           (ast:requires? o))))

(define-method (pc-event? (o <runtime:component>) (return <trigger-return>))
  (let ((port (.port return)))
    (and port
         (let* ((r:port (runtime:port o port))
                (r:other-port (and r:port (runtime:other-port r:port))))
           (or (eq? r:port r:other-port) ;The other port is injected, no
                                        ;chance to find our way back
                                        ;and use ast:injected?
               (and r:other-port
                    (runtime:boundary-port? r:other-port)
                    (ast:provides? r:other-port)))))))

(define-method (pc-event? x y)
  #f)

;;; events formatting

(define-method (trace-name (o <runtime:instance>))
  (cond ((is-a? (%sut) <runtime:port>) #f)
        ((null? (runtime:instance->path o)) "sut")
        ((and (is-a? o <runtime:port>)
              (is-a? (.container o) <runtime:foreign>)
              (runtime:boundary-port?
               (runtime:other-port o)))
         (.name (.ast (runtime:other-port o))))
        (else (string-join (runtime:instance->path o) "."))))

(define-method (pc->event (o <program-counter>))
  (if (and=> (.status o) (negate (is? <end-of-trail>))) (pc->event (.status o))
      (pc->event o (.statement o))))

(define-method (pc->event (o <error>))
  (cons (.ast o) (format #f "<~a>" (or (.message o) "error"))))

(define-method (pc->event (o <blocked-error>))
  (cons #f "<deadlock>"))

(define-method (pc->event (o <match-error>))
  (cons #f "<match>"))

(define-method (pc->event (o <postponed-match>))
  (cons #f "<postponed-match>"))

(define-method (pc->event (pc <program-counter>) (o <statement>))
  (pc->event (.instance pc) o))

(define-method (pc->event (pc <program-counter>) (o <initial-compound>))
  (pc->event (.instance pc) o (.trigger pc)))

(define-method (pc->event (o <runtime:port>) (compound <initial-compound>) (trigger <trigger>))
  (cons trigger
        (if (is-a? (%sut) <runtime:port>)
            (format #f "~a" (.event.name trigger))
            (format #f "~a.~a" (trace-name o) (.event.name trigger)))))

(define-method (pc->event (o <runtime:component>) (compound <initial-compound>) (trigger <trigger>))
  (cons trigger
        (let* ((port (.port trigger))
               (r:other-port (runtime:other-port (runtime:port o port))))
          (format #f "~a.~a" (trace-name r:other-port) (.event.name trigger)))))

(define-method (pc->event (o <runtime:port>) (action <action>))
  (cons action
        (if (ast:out? action)
            (format #f "~a~a" (or (and=> (trace-name o)
                                         (cute format #f "~a." <>))
                                  "")
                    (trigger->string action))
            (format #f "~a" (trigger->string action)))))

(define-method (pc->event (o <runtime:component>) (action <action>))
  (cons action
        (let* ((port (.port action))
               (r:port (runtime:port o port))
               (r:other-port (runtime:other-port r:port)))
          (cond
           ((ast:injected? port)
            (format #f "~a.~a" (trace-name r:other-port) (.event.name action)))
           (else
            (let* ((trigger (action->trigger r:other-port action))
                   (trigger (trigger->string trigger)))
              (format #f "~a.~a" (trace-name r:other-port) trigger)))))))

(define-method (pc->event (o <runtime:component>) (defer-qout <defer-qout>))
  (cons defer-qout "<defer>"))

(define-method (pc->event (o <runtime:component>) (q-in <q-in>))
  (let* ((trigger (.trigger q-in))
         (port (.port trigger))
         (r:port (runtime:port o port))
         (r:other-port (and r:port (runtime:other-port r:port)))
         (trigger (trigger->component-trigger r:other-port trigger)))
    (cons q-in (trigger->string trigger))))

(define-method (pc->event (o <runtime:component>) (q-out <q-out>))
  (let ((trigger (.trigger q-out)))
    (cons q-out (trigger->string trigger))))

(define-method (pc->event (pc <program-counter>) (o <trigger-return>))
  (pc->event (.instance pc) o))

(define-method (pc->event (o <runtime:port>) (return <trigger-return>))
  (cons return
        (let ((value (.event.name return)))
          (if (or (eq? o (%sut)) (not (trace-name o))) (format #f "~a" value)
              (format #f "~a.~a" (trace-name o) value)))))

(define-method (pc->event (o <runtime:component>) (return <trigger-return>))
  (cons return
        (let ((port (.port return)))
          (let* ((r:port (runtime:port o port))
                 (r:other-port (and r:port (runtime:other-port r:port))))
            (if (eq? r:port r:other-port) ;The other port is injected,
                                        ;no chance to find our way back
                                        ;and use ast:injected?
                (format #f "~a.~a" (trace-name r:port) (.event.name return))
                (let ((value (.event.name return)))
                  (format #f "~a.~a" (trace-name r:other-port) value)))))))

(define-method (pc->event x y)
  #f)

;;; component-events formatting

(define-method (pc->component-event (o <program-counter>))
  (if (and=> (.status o) (negate (is? <end-of-trail>))) (pc->component-event (.status o))
      (pc->component-event o (.statement o))))

(define-method (pc->component-event (pc <program-counter>) (o <statement>))
  (pc->component-event (.instance pc) o))

(define-method (pc->component-event (pc <program-counter>) (o <initial-compound>))
  (pc->component-event (.instance pc) o (.trigger pc)))

(define-method (pc->component-event (o <runtime:port>) (compound <initial-compound>) (trigger <trigger>))
  (cons trigger
        (if (is-a? (%sut) <runtime:port>)
            (format #f "~a" (.event.name trigger))
            (format #f "~a.~a" (.name (.ast o)) (.event.name trigger)))))

(define-method (pc->component-event (o <runtime:component>) (compound <initial-compound>) (trigger <trigger>))
  (cons trigger
        (let* ((port (.port trigger))
               (r:port (runtime:port o port)))
          (format #f "~a.~a" (trace-name r:port) (.event.name trigger)))))

(define-method (pc->component-event (o <runtime:component>) (action <action>))
  (cons action
        (let* ((port (.port action))
               (r:port (runtime:port o port)))
          (cond
           ((ast:injected? port)
            (format #f "~a.~a" (trace-name r:port) (.event.name action)))
           (else
            (format #f "~a.~a" (trace-name r:port) (.event.name action)))))))

(define-method (pc->component-event (pc <program-counter>) (o <trigger-return>))
  (pc->component-event (.instance pc) o))

(define-method (pc->component-event (o <runtime:component>) (return <trigger-return>))
  (cons return
        (let ((port (.port return)))
          (let* ((r:port (runtime:port o port))
                 (r:other-port (and r:port (runtime:other-port r:port))))
            (if (eq? r:port r:other-port) ;The other port is injected
                (format #f "~a.~a" (trace-name r:port) (.event.name return))
                (let ((value (.event.name return)))
                  (format #f "~a.~a" (trace-name r:port) value)))))))

(define-method (pc->component-event x)
  (pc->event x))

(define-method (pc->component-event x y)
  (pc->event x y))


;;;
;;; Trace (a.k.a. arrows, broken arrows)
;;;

(define-method (trace->arrows (o <list>))
  (filter-map trace->arrows (reverse o)))

(define-method (trace->arrows (o <program-counter>))
  (and (pc-arrow? o) (pc->arrow o)))

;;; arrow predicate

(define-method (pc-arrow? (o <program-counter>))
  (if (and=> (.status o) (negate (is? <end-of-trail>))) (pc-event? o)
      (pc-arrow? o (.statement o))))

(define-method (pc-arrow? (pc <program-counter>) (o <statement>))
  (pc-arrow? (.instance pc) o))

(define-method (pc-arrow? (pc <program-counter>) (o <initial-compound>))
  (pc-arrow? (.instance pc) (.trigger pc)))

(define-method (pc-arrow? (o <runtime:port>) (trigger <trigger>))
  (not (ast:modeling? trigger)))

(define-method (pc-arrow? (o <runtime:port>) (trigger <synth-trigger>))
  #t)

(define-method (pc-arrow? (o <runtime:component>) (trigger <trigger>))
  #t)

(define-method (pc-arrow? (o <runtime:component>) (trigger <q-trigger>))
  #f)

(define-method (pc-arrow? (o <runtime:port>) (action <action>))
  (or (ast:in? action)
      (is-a? (%sut) <runtime:port>)
      (not (and (ast:requires? o)
                (ast:external? o)))))

(define-method (pc-arrow? (o <runtime:component>) (action <action>))
  #t)

(define-method (pc-arrow? (o <runtime:component>) (action <defer-qout>))
  #t)

(define-method (pc-arrow? (o <runtime:port>) (q-out <q-out>))
  #t)

(define-method (pc-arrow? (o <runtime:component>) (q-out <q-out>))
  #t)

(define-method (pc-arrow? (pc <program-counter>) (o <trigger-return>))
  (and (not (is-a? (.event (.trigger pc)) <modeling-event>))
       (pc-arrow? (.instance pc) o)))

(define-method (pc-arrow? (o <runtime:port>) (return <trigger-return>))
  (let ((on (ast:parent return <on>)))
    (and on
         (let ((trigger (car (ast:trigger* on))))
           (or (is-a? (%sut) <runtime:port>)
               (not (ast:modeling? trigger)))))))

(define-method (pc-arrow? (o <runtime:component>) (return <trigger-return>))
  (.port return))

(define-method (pc-arrow? x y)
  (pc-event? x y))

;;; arrow formatting

(define-method (pc->arrow (o <program-counter>))
  (cons o
        (if (and=> (.status o) (negate (is? <end-of-trail>))) (pc->event (.status o))
            (pc->arrow o (.statement o)))))

(define-method (pc->arrow (pc <program-counter>) (o <statement>))
  (pc->arrow (.instance pc) o))

(define-method (pc->arrow (pc <program-counter>) (o <initial-compound>))
  (pc->arrow (.instance pc) o (.trigger pc)))

(define-method (pc->arrow (o <runtime:port>) (compound <initial-compound>) (trigger <trigger>))
  (cons trigger
        (if (or (ast:provides? o)
                (and (is-a? (%sut) <runtime:port>)
                     (not (eq? o (%sut)))))
            (format #f "~a.~a -> ..." (runtime:instance->string o) (.event.name trigger))
            (format #f "... -> ~a.~a" (runtime:instance->string o) (.event.name trigger)))))

(define-method (pc->arrow (o <runtime:port>) (compound <initial-compound>) (trigger <synth-trigger>))
  (cons trigger
        (if (or (ast:provides? o)
                (and (is-a? (%sut) <runtime:port>)
                     (not (eq? o (%sut)))))
            (format #f "... <- ~a.~a" (runtime:instance->string o) (.event.name trigger))
            (format #f "~a.~a <- ..." (runtime:instance->string o) (.event.name trigger)))))

(define-method (pc->arrow (o <runtime:component>) (compound <initial-compound>) (trigger <trigger>))
  (cons trigger
        (let* ((port (.port trigger))
               (r:port (runtime:port o port))
               (r:other-port (runtime:other-port r:port)))
          (cond
           ((eq? r:port r:other-port) ; injected
            (format #f "... -> ~a.~a" (runtime:instance->string r:port) (.event.name trigger)))
           (else
            (format #f "... -> ~a.~a" (runtime:instance->string r:port) (.event.name trigger)))))))

(define-method (pc->arrow (o <runtime:port>) (action <action>))
  (cons action
        (if (or (ast:provides? o)
                (and (is-a? (%sut) <runtime:port>)
                     (not (eq? o (%sut)))))
            (format #f "~a.~a <- ..." (runtime:instance->string o) (.event.name action))
            (format #f "... <- ~a.~a" (runtime:instance->string o) (.event.name action)))))

(define-method (pc->arrow (o <runtime:component>) (action <action>))
  (cons action
        (let* ((port (.port action))
               (r:port (runtime:port o port))
               (r:other-port (runtime:other-port r:port))
               (trigger (and r:other-port
                             (action->trigger r:other-port action))))
          (cond
           ((not trigger)
            (format #f "~a.~a -> ..." (runtime:instance->string r:port) (.event.name action)))
           ((ast:injected? port)
            (format #f "~a.~a -> ..." (runtime:instance->string r:port) (.event.name action)))
           ((ast:out? action)
            (format #f "... <- ~a.~a" (runtime:instance->string r:port) (.event.name trigger)))
           (else
            (format #f "~a.~a -> ..." (runtime:instance->string r:port) (.event.name trigger)))))))

(define-method (pc->arrow (o <runtime:component>) (defer-qout <defer-qout>))
  (cons defer-qout
        (format #f "<defer>")))

(define-method (pc->arrow (o <runtime:port>) (q-in <q-in>))
  (cons q-in
        (let ((trigger (.trigger q-in)))
          (format #f "... <- ~a.~a" (runtime:instance->string o) (.event.name trigger)))))

(define-method (pc->arrow (o <runtime:component>) (q-in <q-in>))
  (cons q-in (format #f "~a.<q> <- ..." (runtime:instance->string o))))

(define-method (pc->arrow (o <runtime:port>) (q-out <q-out>))
  (cons q-out
        (let* ((r:other-port (runtime:other-port o))
               (r:other-instance (.container r:other-port)))
          (format #f "... <- ~a.<q>" (runtime:instance->string r:other-instance)))))

(define-method (pc->arrow (o <runtime:component>) (q-out <q-out>))
  (cons q-out
        (let* ((trigger (.trigger q-out))
               (r:port (runtime:port o (.port trigger))))
          (format #f "~a.~a <- ..." (runtime:instance->string r:port) (.event.name trigger)))))

(define-method (pc->arrow (pc <program-counter>) (o <trigger-return>))
  (pc->arrow (.instance pc) o))

(define-method (pc->arrow (o <runtime:port>) (return <trigger-return>))
  (cons return
        (let ((value (.event.name return)))
          (cond ((ast:eq? o (%sut))
                 (format #f "... <- ~a.~a" (runtime:instance->string o) value))
                ((or (ast:provides? o)
                     (and (is-a? (%sut) <runtime:port>)
                          (not (eq? o (%sut)))))
                 (format #f "~a.~a <- ..." (runtime:instance->string o) value))
                (else
                 (format #f "... <- ~a.~a" (runtime:instance->string o) value))))))

(define-method (pc->arrow (o <runtime:component>) (return <trigger-return>))
  (cons return
        (let ((port (.port return)))
          (let* ((r:port (and port (runtime:port o port)))
                 (r:other-port (and r:port (runtime:other-port r:port)))
                 (value (.event.name return))                 )
            (cond
             ((ast:injected? port)
              (format #f "~a.~a <- ..." (runtime:instance->string r:port) (.event.name return)))
             ((and r:port (eq? r:port r:other-port)) ;injected
              (format #f "... <- ~a.~a" (runtime:instance->string r:port) (.event.name return)))
             ((ast:provides? port)
              (format #f "... <- ~a.~a" (runtime:instance->string r:port) value))
             (else
              (format #f "~a.~a <- ..." (runtime:instance->string r:port) value)))))))

(define-method (pc->arrow x y)
  #f)

(define-method (pc->arrow x)
  #f)


;;;
;;; Steps (a.k.a. micro steps, arrows with assignments, calls, ...
;;;

(define-method (trace->steps (o <list>))
  (filter-map trace->steps (reverse o)))

(define-method (trace->steps (o <program-counter>))
  (and (pc-step? o) (pc->step o)))

;;; step predicate

(define-method (pc-step? (o <program-counter>))
  (if (and=> (.status o) (negate (is? <end-of-trail>))) (pc-event? o)
      (pc-step? o (.statement o))))

(define-method (pc-step? (pc <program-counter>) (o <initial-compound>))
  (pc-arrow? (.instance pc) (.trigger pc)))

(define-method (pc-step? (pc <program-counter>) (o <statement>))
  #t)

(define-method (pc-step? (pc <program-counter>) (o <compound>))
  #f)

(define-method (pc-step? (pc <program-counter>) (o <declarative-compound>))
  #f)

(define-method (pc-step? (pc <program-counter>) (o <on>))
  #f)

(define-method (pc-step? (pc <program-counter>) (o <end-of-on>))
  #f)

(define-method (pc-step? (pc <program-counter>) (o <flush-return>))
  #f)

(define-method (pc-step? x y)
  (pc-arrow? x y))

(define-method (pc-step? x)
  (pc-arrow? x))

;;; step formatting

(define-method (pc->step (o <program-counter>))
  (cons o
        (if (and=> (.status o) (negate (is? <end-of-trail>))) (pc->event (.status o))
            (pc->step o (.statement o)))))

(define-method (pc->step (pc <program-counter>) (o <guard>))
  (cons o (format #f "[~a]" (string-trim-both (ast->dzn (.expression o))))))

(define-method (pc->step (pc <program-counter>) (o <statement>))
  (or (pc->arrow pc o)
      (cons o (string-trim-both (ast->dzn o)))))

(define-method (pc->step x y)
  (pc->arrow x y))


;;;
;;; Report
;;;

(define (complete-split-arrows-pcs trace)
  "The split-arrows format needs a PC for both ends of the arrow.
Add (synthesize) missing PCs for <q-in>, <q-out> and <trigger-return>."

  (define (action-matches? r:port pc)
    (let ((statement (.statement pc)))
      (and (is-a? statement <action>)
           (eq? r:port (.instance pc))
           statement)))

  (define (external-triggers pc)
    (append-map cdr (.external-q pc)))

  (define (injected-port-name component interface)
    (let* ((ports (filter ast:injected? (ast:port* component)))
           (port (find (compose (cute ast:eq? <> interface) .type) ports)))
      (and=> port .name)))

  (let loop ((trace trace) (result '()))
    (if (null? trace) (reverse result)
        (let* ((next (and (pair? result) (car result)))
               (pc (car trace))
               (pc-instance (.instance pc))
               (statement (.statement pc)))
          (cond
           ((.status pc)
            (loop (cdr trace) (cons pc result)))
           ((and (pair? result)
                 ((is-status? <compliance-error>) (last result))
                 (is-a? statement <action>)
                 (not (.component-acceptance (.status (last result))))
                 (.port-acceptance (.status (last result))))
            (let* ((last-pc (last result))
                   (status (.status last-pc))
                   (r:port (.port status))
                   (port (.ast r:port))
                   (interface (.type port))
                   (trigger (.trigger pc))
                   (trigger (make <synth-trigger> #:event.name (.event.name trigger)))
                   (trigger (clone trigger #:parent interface))
                   (initial-compound (make <initial-compound>))
                   (initial-compound (clone initial-compound #:parent interface))
                   (trigger-pc (clone pc
                                      #:trigger trigger
                                      #:statement initial-compound)))
              (loop (cdr trace) (cons* trigger-pc pc result))))
           ((and (is-a? (%sut) <runtime:port>)
                 (is-a? statement <initial-compound>))
            (let ((client-pc (clone pc #:instance (car (%instances)))))
              (loop (cdr trace) (cons* client-pc pc result))))
           ((and (is-a? (%sut) <runtime:port>)
                 (is-a? statement <action>))
            (let ((client-pc (clone pc #:instance (car (%instances)))))
              (loop (cdr trace) (cons* pc client-pc result))))
           ((and (is-a? (%sut) <runtime:port>)
                 (is-a? statement <trigger-return>))
            (let ((client-pc (clone pc #:instance (car (%instances)))))
              (loop (cdr trace) (cons* pc client-pc result))))
           ((and next
                 (is-a? statement <action>)
                 (is-a? (.trigger next) <q-trigger>)
                 (is-a? pc-instance <runtime:component>)
                 (let* ((port (.port statement))
                        (r:port (runtime:port pc-instance port))
                        (r:other-port (and r:port (runtime:other-port r:port))))
                   (and r:other-port
                        (ast:provides? r:other-port))))
            (let* ((port (.port statement))
                   (r:port (runtime:port pc-instance port))
                   (r:other-port (and r:port (runtime:other-port r:port)))
                   (action (clone statement #:port.name #f))
                   (other-port (.ast r:other-port))
                   (other-interface (.type other-port))
                   (action (clone action #:parent other-interface))
                   (action-pc (clone pc #:instance r:other-port #:statement action)))
              (loop (cdr trace) (cons* pc action-pc result))))
           ((and next
                 (is-a? statement <action>)
                 (ast:out? statement)
                 (.instance next)
                 (.deferred (get-state next))
                 (pair? (.q (get-state next (.deferred (get-state next))))))
            (let* ((deferred (.deferred (get-state next)))
                   (trigger (car (.q (get-state next deferred))))
                   (q-in (make <q-in> #:trigger trigger #:location (.location trigger)))
                   (q-in (clone q-in #:parent (.ast deferred)))
                   (q-pc (clone pc #:instance deferred #:statement q-in)))
              (loop (cdr trace) (cons* pc q-pc result))))
           ((and (is-a? statement <action>)
                 (ast:out? statement)
                 (is-a? pc-instance <runtime:component>)
                 (or (not next)
                     (not (is-a? (.statement next) <action>))
                     (not (equal? (.event.name (.statement next))
                                  (.event.name statement)))
                     (let* ((port (.port statement))
                            (r:port (runtime:port pc-instance port))
                            (r:other-port (runtime:other-port r:port)))
                       (not (eq? r:other-port (.instance next))))))
            (let* ((port (.port statement))
                   (r:port (runtime:port pc-instance port))
                   (r:other-port (and r:port (runtime:other-port r:port)))
                   (interface (.type port))
                   (action (clone statement
                                  #:port.name #f
                                  #:location (.location statement)))
                   (action (clone action #:parent interface))
                   (action-pc (clone pc
                                     #:instance r:other-port
                                     #:statement action)))
              (loop (cdr trace) (cons* pc action-pc result))))
           ((and (is-a? statement <q-in>)
                 (is-a? pc-instance <runtime:component>))
            (let* ((trigger (.trigger statement))
                   (port (.port trigger))
                   (r:port (runtime:port pc-instance port))
                   (r:other-port (and r:port (runtime:other-port r:port)))
                   (other-port (.ast r:other-port))
                   (other-interface (.type other-port))
                   (trigger (clone trigger #:port.name #f))
                   (qin (clone statement #:trigger trigger))
                   (qin (clone qin #:parent other-interface))
                   (qin-pc (clone pc #:instance r:other-port #:statement qin)))
              (loop (cdr trace) (cons* qin-pc pc result))))
           ((and (is-a? statement <q-out>)
                 (is-a? pc-instance <runtime:component>))
            (let* ((trigger (.trigger statement))
                   (port (.port trigger))
                   (r:port (runtime:port pc-instance port))
                   (r:other-port (and r:port (runtime:other-port r:port)))
                   (q-trigger (clone trigger #:port.name #f))
                   (on (find (conjoin
                              (compose (is? <on>) .statement)
                              (compose (cute eq? <> pc-instance) .instance))
                             result))
                   (on (and on (.statement on)))
                   (location (if on (.location on) (.location statement)))
                   (pc (clone pc #:statement (clone statement #:location location)))
                   (q-out (clone statement #:trigger q-trigger #:location location))
                   (other-port (.ast r:other-port))
                   (other-interface (.type other-port))
                   (q-out (clone q-out #:parent other-interface))
                   (q-pc (clone pc #:instance r:other-port #:statement q-out)))
              (loop (cdr trace) (cons* q-pc pc result))))
           ((and next
                 (not (is-a? (%sut) <runtime:port>))
                 (is-a? statement <trigger-return>)
                 (is-a? (.instance next) <runtime:component>)
                 (or (and (is-a? pc-instance <runtime:port>)
                          (ast:requires? pc-instance))
                     (and (is-a? pc-instance <runtime:component>)
                          (let* ((port (.port statement))
                                 (r:port (if (is-a? pc-instance <runtime:port>) pc-instance
                                             (runtime:port pc-instance port)))
                                 (r:other-port (runtime:other-port r:port)))
                            (or (ast:requires? r:other-port)
                                (eq? r:port r:other-port))))) ; injected
                 (not (ast:modeling? (car (ast:trigger* (ast:parent statement <on>))))))
            (let* ((next-statement (.statement next)) ;; XXX statement *after* action
                   (next-instance (.instance next))
                   (port (.port statement))
                   (r:port (if (is-a? pc-instance <runtime:port>) pc-instance
                               (runtime:port pc-instance port)))
                   (r:other-port (runtime:other-port r:port))
                   (interface (if port (.type port) (ast:parent statement <interface>)))
                   (component (.type (.ast next-instance)))
                   (port-name (if (or (not r:other-port) (eq? r:port r:other-port)) (injected-port-name component interface)
                                  (.name (.ast r:other-port))))
                   (action (and r:other-port (any (cute action-matches? (.container r:other-port) <>) trace)))
                   (return (clone statement
                                  #:port.name port-name
                                  #:location (.location (or action statement))))
                   (return (clone return #:parent component))
                   (return-pc (clone pc #:instance next-instance #:statement return)))
              (loop (cdr trace) (cons* pc return-pc result))))
           (else
            (loop (cdr trace) (cons pc result))))))))

(define (set-trigger-locations trace)
  "The trigger in the TRACEs PCs are synthesized; set their location to
the location of the executed <on>-statement."
  (let loop ((trace trace))
    (if (null? trace) '()
        (let* ((pc (car trace))
               (trigger (.trigger pc))
               (model (and trigger (ast:parent trigger <model>)))
               (on (and=> (find (conjoin (compose (is? <on>) .statement)
                                         (compose (cute ast:eq? <> model)
                                                  (cute ast:parent <> <model>)
                                                  .statement))
                                trace)
                          .statement))
               (triggers (and on (ast:trigger* on)))
               (on-trigger (and triggers
                                (find (cute ast:equal? <> trigger) triggers)))
               (trigger (and trigger on-trigger
                             (clone trigger #:location (.location on-trigger))))
               (pc (if (not trigger) pc (clone pc #:trigger trigger))))
          (cons pc (loop (cdr trace)))))))

(define (location-prefix o)
  (format #f "~a: " (or (and=> o ast:location->string) "<unknown-file>")))

(define* (display-trace trace #:key locations? state? verbose?)
  "Write TRACE as split-arrows trace to stdout.  When LOCATIONS?,
prepend every line with its location.  When VERBOSE?, also show
intermediate steps such as assignments, function calls, replies,
... (aka micro-trace)."
  (let* ((trace (complete-split-arrows-pcs trace))
         (trace (reverse (set-trigger-locations (reverse trace))))
         (steps (if verbose? (trace->steps trace)
                    (trace->arrows trace)))
         (max (length steps)))
    (define write-step
      (match-lambda*
        (((pc ast . string) i)
         (when (and ast locations?)
           (let ((location (location-prefix ast)))
             (when location
               (display location))))
         (write-line string)
         (when (and state?
                    (odd? i)
                    (< i (- max 2)))
           (serialize (.state pc) (current-output-port))
           (newline)))))
    (define (arrow? string)
      (or (string-contains string "<-")
          (string-contains string "->")))
    (let loop ((steps steps) (i 0))
      (match steps
        (() #t)
        (((and step (pc ast . string)) steps ...)
         (write-step step i)
         (let ((i (if (arrow? string) (1+ i) i)))
           (loop steps i)))))))

(define (initial-error-message traces)
  (let* ((pcs (map car traces))
         (status (.status (car pcs))))
    (match status
      (($ <compliance-error>)
       (let* ((component-acceptance (.component-acceptance status))
              (port-acceptances (.port-acceptance status))
              (port-acceptances (and port-acceptances
                                     (ast:acceptance* port-acceptances)))
              (port-acceptance (and (pair? port-acceptances) (car port-acceptances)))
              (location (location-prefix (or component-acceptance
                                             port-acceptance)))
              (message (.message status)))
         (format #f "~aerror: ~a\n" location message)))
      (($ <fork-error>)
       (let ((location (and=> (.ast status) location-prefix))
             (message (.message status)))
         (format #f "~aerror: ~a\n" location message)))
      (($ <refusals-error>)
       (let ((location (and=> (.ast status) location-prefix))
             (message (.message status)))
         (format #f "~aerror: ~a\n" location message)))
      ((and (? (is? <determinism-error>)))
       (let* ((ast (or (.ast status)
                       (.behavior (runtime:%sut-model))))
              (locations (map (lambda (pc)
                                (or (and=> (.ast (.status pc)) location-prefix)
                                    (location-prefix ast)))
                              pcs))
              (model (ast:parent ast <model>))
              (message (if (is-a? model <component>)
                           (simple-format
                            #f "component ~s is non-deterministic"
                            (ast:dotted-name model))
                           (simple-format
                            #f "interface ~s is unobservably non-deterministic"
                            (ast:dotted-name model)))))
         (string-join
          (map (cut format #f "~aerror: ~a\n" <> message) locations) "")))
      (($ <queue-full-error>)
       (let* ((model ((compose ast:dotted-name .type .ast .instance) status))
              (location (location-prefix (.ast status))))
         (format #f "~aerror: queue-full in component ~s\n" location model)))
      (($ <range-error>)
       (let* ((variable (.variable status))
              (name (.name variable))
              (type (.type variable))
              (value (.value status))
              (location (location-prefix (.ast status))))
         (format #f "~aerror: range-error: invalid value `~a' for variable `~a'\n" location value name)))
      ((and (? (is? <missing-reply-error>)))
       (let* ((type (.type status))
              (location (location-prefix (.ast status))))
         (format #f "~aerror: missing-reply: ~a reply expected\n" location (type-name type))))
      (($ <second-reply-error>)
       (let* ((previous (.previous status))
              (location (location-prefix (.ast status))))
         (format #f "~aerror: second-reply\n" location)))
      ((and (? (is? <match-error>)) (= .ast ast))
       (let ((location (location-prefix ast)))
         (format #f "~aerror: ~a\n" location (.input status))))
      ((and (? (is? <error>)) (= .ast ast) (= .message message))
       (let ((location (location-prefix ast)))
         (format #f "~aerror: ~a\n" location message)))
      (($ <end-of-trail>) #f)
      (_ #f #f))))

(define (final-error-messages traces)
  (let* ((pcs (map car traces))
         (pc (car pcs))
         (status (.status pc)))
    (match status
      (($ <compliance-error>)
       (let* ((component-acceptance (.component-acceptance status))
              (port-acceptances (.port-acceptance status))
              (port-acceptances (and port-acceptances
                                     (ast:acceptance* port-acceptances)))
              (port-acceptance (and (pair? port-acceptances) (car port-acceptances)))
              (location (location-prefix (or component-acceptance
                                             port-acceptance)))
              (acceptance (if component-acceptance (trigger->string component-acceptance)
                              "-"))
              (r:port (.port status))
              (port-name (and r:port (string-join (runtime:instance->path r:port) ".")))
              (component-name (string-join (runtime:instance->path (or (.instance (car pcs)) (%sut))) "."))
              (trigger (.trigger status)))
         (string-join
          (append
           (if (not trigger) '()
               (let ((location (location-prefix (.event trigger))))
                 (if (not location) '()
                     (list (format #f "~atrigger: ~a\n" location
                                   (trigger->string trigger))))))
           (list (format #f "~acomponent performs: ~a\n" location acceptance))
           (if (not port-acceptances)
               (let* ((interface (.type (.ast r:port)))
                      (ast (.behavior interface)))
                 (list (format #f "~aport expects: -\n" (location-prefix ast))))
               (map
                (lambda (ast)
                  (if ast
                      (format #f "~aport expects: ~a\n" (location-prefix ast) (trigger->string ast))
                      (let* ((ast ((compose .statement .behavior .type .ast .port) status))
                             (location (location-prefix ast)))
                        (format #f "~aport expects: ~a\n"  location "-"))))
                port-acceptances)))
          "")))
      (($ <fork-error>)
       (let* ((action (.ast status))
              (location (location-prefix action))
              (action-port (.port action))
              (action-port-name (.name action-port))
              (first (.action status)))
         (string-join
          (list
           (format #f "~aforking not allowed, action on port: ~a\n" location action-port-name)
           (if first
               (let* ((first-location (location-prefix first))
                      (first-port-name (.name (.port first))))
                 (format #f "~aafter action on port: ~a\n" first-location first-port-name))
               (let* ((trigger (.trigger pc))
                      (trigger-location (location-prefix (.statement pc)))
                      (trigger-port (.port trigger))
                      (trigger-port-name (.name trigger-port)))
                 (format #f "~afor trigger on port ~a\n" trigger-location trigger-port-name))))
          "")))
      (($ <refusals-error>)
       (match (.refusals status)
         ((and ($ <trigger>) trigger)
          (let ((location (location-prefix trigger))
                (trigger (trigger->string trigger)))
            (format #f "~acomponent can omit return of trigger: ~a\n"
                    location trigger)))
         (_
          (let* ((refusals (sort (.refusals status) equal?))
                 (refusals (apply append refusals))
                 (location (and=> (.ast status) location-prefix))
                 (inevitables optionals
                              (partition
                               (compose (cute equal? "inevitable" <>) car)
                               refusals)))
            (string-append
             (format #f "~acomponent can omit all of: ~a\n"
                     location
                     (string-join (map cadr refusals) ", "))
             (format #f "~athe interface promises inevitably: ~a \n"
                     location
                     (string-join (map cadr inevitables) ","))
             (if (null? optionals) ""
                 (format #f "~aor optionally: ~a\n"
                         location
                         (string-join (map cadr optionals) ","))))))))
      (($ <range-error>)
       (let* ((variable (.variable status))
              (name (.name variable))
              (type (.type variable))
              (value (.value status))
              (location (location-prefix (.ast status))))
         (string-join
          (list
           (format #f "~ainfo: ~a `~a' defined here\n" (location-prefix variable) (ast-name variable) name)
           (format #f "~ainfo: of type `~a' defined here\n" (location-prefix type) (ast:name type)))
          "")))
      (($ <second-reply-error>)
       (let* ((previous (.previous status))
              (location (location-prefix (.ast status))))
         (format #f "~ainfo: reply previously set here\n" (location-prefix previous))))
      ((and (? (is? <match-error>)) (= .ast #f))
       (let ((location (location-prefix #f)))
         (format #f "~aerror: no match; got input `~s'\n" location (.input status))))
      ((and (? (is? <match-error>)) (= .ast ast))
       (let ((location (location-prefix ast))
             (label (label->string ast)))
         (if label
             (format #f "~aerror: no match; at `~s', got input `~s'\n"
                     location label (.input status))
             (format #f "~aerror: no match; got input `~s'\n"
                     location (.input status)))))
      ((and ($ <end-of-trail> ) (and (= .ast ast)))
       (and ast
            (let* ((instance (.instance (car pcs)))
                   (port-name (if instance (string-join (runtime:instance->path instance) ".")
                                  (.port.name ast)))
                   (instance (if instance (runtime:instance->string instance)
                                 port-name))
                   (location (location-prefix (.ast status)))
                   (prefix (if (is-a? (%sut) <runtime:port>) ""
                               (string-append instance ".")))
                   (ast (trigger->string (clone ast #:port.name #f))))
              (format #f "~ainfo: end of trail; stopping here: ~a~a\n" location prefix ast))))
      (_ #f))))

(define* (report traces #:key eligible (trace "event") internal? locations? state? verbose?)
  "Print any error messages and the trail of (car TRACES) in
trace-format TRACE, given that the trail is the same for all TRACES."

  (define (trace< a b)
    "Compare traces A and B on trail length, trace length or string
value of the trail."
    (define (length< a b)
      (< (length a) (length b)))
    (let* ((trail-a (trace->string-trail a))
           (trail-b (trace->string-trail b))
           (result (length< trail-a trail-b)))
      (if (or result (length< trail-b trail-a)) result
          (let ((result (length< trail-a trail-b)))
            (if (or result (length< trail-b trail-a)) result
                (string< (string-join trail-a)
                         (string-join trail-b)))))))

  (let* ((error traces (partition (compose (is? <error>) .status car) traces))
         (traces (if (null? error) traces error))
         (traces (sort traces trace<))
         (trace-format trace)
         (trace (and (pair? traces) (car traces)))
         (pc (and trace (car trace)))
         (status (and pc (.status pc)))
         (initial-message (and status (initial-error-message traces))))
    (when initial-message
      (display initial-message)
      (display initial-message (current-error-port)))

    (cond ((null? traces))
          ((equal? trace-format "event")
           (display-trail trace))
          ((equal? trace-format "trace")
           (display-trace trace
                          #:locations? locations? #:state? state? #:verbose? verbose?))
          ((equal? trace-format "diagram")
           (let ((split-arrows
                  (with-output-to-string
                    (lambda _
                      (serialize-header (.state pc) (current-output-port))
                      (newline)
                      (serialize (.state pc) (current-output-port))
                      (newline)
                      (display-trace trace
                                     #:locations? locations? #:state? state? #:verbose? verbose?)))))
             (display (trace:format-trace split-arrows #:format "diagram" #:internal? internal?)))))

    (when (and pc
               (not (equal? trace-format "diagram")))
      (serialize (.state pc) (current-output-port))
      (newline))

    (when (and (equal? trace-format "trace") initial-message)
      (display initial-message))
    (when (pair? traces)
      (let ((final-messages (final-error-messages traces)))
        (when final-messages
          (display final-messages (current-error-port)))
        (when (equal? trace-format "trace")
          (let* ((trail (trace->trail trace))
                 (trail (map cdr trail)))
            (when (pair? trail)
              (format #t "~s\n" (cons 'trail trail)))))))

    (when (and (equal? trace-format "trace")
               eligible)
      (let* ((end-of-trail? (and (is-a? status <end-of-trail>) (.ast status)))
             (end-of-trail (and end-of-trail? (end-of-trail-labels pc)))
             (eligible (if (not end-of-trail?) eligible
                           end-of-trail))
             (labels (or (and=> pc labels) '()))
             (labels (if (pair? end-of-trail) eligible
                         labels)))
        (format #t "~s\n" (cons 'labels labels))
        (format #t "~s\n" (cons 'eligible eligible))))

    status))
