;;; Dezyne --- Dezyne command line tools
;;;
;;; Copyright © 2021, 2022, 2023, 2024 Janneke Nieuwenhuizen <janneke@gnu.org>
;;; Copyright © 2021, 2022, 2023 Rutger van Beusekom <rutger@dezyne.org>
;;; Copyright © 2021 Paul Hoogendijk <paul@dezyne.org>
;;;
;;; This file is part of Dezyne.
;;;
;;; Dezyne is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Dezyne is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (dzn commands graph)
  #:use-module (srfi srfi-26)

  #:use-module (ice-9 getopt-long)

  #:use-module (dzn ast)
  #:use-module (dzn code)
  #:use-module (dzn command-line)
  #:use-module (dzn commands parse)
  #:use-module (dzn code)
  #:use-module (dzn config)
  #:use-module (dzn explore)
  #:use-module (dzn parse)

  #:export (parse-opts
            main))

(define (parse-opts args)
  (let* ((option-spec
          '((backend (single-char #\b) (value #t))
            (format (single-char #\f) (value #t))
            (help (single-char #\h))
            (hide (single-char #\H) (value #t))
            (import (single-char #\I) (value #t))
            (locations (single-char #\L))
            (model (single-char #\m) (value #t))
            (queue-size (single-char #\q) (value #t))
            (queue-size-defer (value #t))
            (queue-size-external (value #t))
            (remove (single-char #\R) (value #t))))
         (options (getopt-long args option-spec))
         (help? (option-ref options 'help #f))
         (files (option-ref options '() '()))
         (usage? (and (not help?) (null? files))))
    (when (or help? usage?)
      (let ((port (if usage? (current-error-port) (current-output-port))))
        (format port "\
Usage: dzn graph [OPTION]... [FILE]...
Generate graph from a Dezyne model

  -b, --backend=TYPE     write a graph of TYPE to stdout [system]
                           {dependency,lts,state,system}
  -f, --format=FORMAT    produce graph in format FORMAT [dot] {aut,dot,json}
  -h, --help             display this help and exit
  -H, --hide=HIDE        hide from transitions HIDE {labels,actions,returns}
                           implies --backend=state
  -I, --import=DIR+      add DIR to import path
  -L, --locations        include locations in graph
  -m, --model=MODEL      produce graph for MODEL
  -q, --queue-size=SIZE  use queue size=SIZE for exploration [~a]
      --queue-size-defer=SIZE
                         use defer queue size=SIZE for verification [~a]
      --queue-size-external=SIZE
                         use external queue size=SIZE for verification [~a]
  -R, --remove=VARS      remove state from nodes VARS {ports,extended}
                           implies --backend=state

"
                (%queue-size) (%queue-size-defer) (%queue-size-external))
        (exit (or (and usage? EXIT_OTHER_FAILURE) EXIT_SUCCESS))))
    options))

(define (main args)
  (let* ((options (parse-opts args))
         (files (option-ref options '() '()))
         (file-name (car files))
         (model-name (option-ref options 'model #f))
         (remove (command-line:get 'remove #f))
         (ports? (equal? remove "ports"))
         (extended? (equal? remove "extended"))
         (hide (command-line:get 'hide #f))
         (actions? (equal? hide "actions"))
         (labels? (equal? hide "labels"))
         (returns? (equal? hide "returns"))
         (backend (option-ref options 'backend
                              (if (or hide remove) "state"
                                  "system")))
         (dependency? (equal? backend "dependency"))
         (lts? (equal? backend "lts"))
         (state? (equal? backend "state"))
         (debug? (dzn:command-line:get 'debug #f))
         (locations? (option-ref options 'locations #f))
         ;; Parse --model=MODEL cuts MODEL from AST; avoid that
         (parse-options (filter (negate (compose (cute eq? <> 'model) car))
                                options))
         (ast (parameterize ((%language "makreel"))
                (parse parse-options file-name)))
         (model (call-with-handle-exceptions
                 (lambda _ (ast:get-model ast model-name))
                 #:backtrace? debug?
                 #:file-name file-name))
         (language (option-ref options 'format "dot"))
         (queue-size (command-line:get-number 'queue-size (%queue-size)))
         (queue-size-defer (command-line:get-number 'queue-size-defer
                                                    (%queue-size-defer)))
         (queue-size-external (command-line:get-number 'queue-size-external
                                                       (%queue-size-external))))
    (when (and hide
               (not (member hide '("actions" "labels" "returns"))))
      (format (current-error-port) "graph: hide ~a ignored\n" hide))
    (when (and remove
               (not (member remove '("ports" "extended"))))
      (format (current-error-port) "graph: remove ~a ignored\n" remove))
    (unless model
      (format (current-error-port) "~a: No dezyne model found.\n" file-name)
      (exit EXIT_OTHER_FAILURE))
    (cond (dependency? (code ast
                             #:ast-> 'dependency-diagram
                             #:model model
                             #:language language))
          (lts? (lts ast
                     #:model model
                     #:queue-size queue-size
                     #:queue-size-defer queue-size-defer
                     #:queue-size-external queue-size-external))
          (state? (state-diagram ast
                                 #:format language
                                 #:model model
                                 #:queue-size queue-size
                                 #:queue-size-defer queue-size-defer
                                 #:queue-size-external queue-size-external
                                 #:ports? ports?
                                 #:extended? extended?
                                 #:actions? actions?
                                 #:labels? labels?
                                 #:returns? returns?))
          (else (code ast
                      #:ast-> 'system-diagram
                      #:dir "-"
                      #:model model
                      #:language language
                      #:locations? locations?)))))
