# Dezyne --- Dezyne command line tools
#
# Copyright © 2021 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
# Copyright © 2021, 2022 Rutger van Beusekom <rutger@dezyne.org>
#
# This file is part of Dezyne.
#
# Dezyne is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Dezyne is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
#
# Commentary:
#
# Code:

EXTRA_DIST += %D%/README

dist_info_images_DATA =					\
 %D%/Camera-state.png					\
 %D%/Camera-system.png					\
 %D%/LegoBallSorter-system.png				\
 %D%/MaterialHandler-system.png				\
 %D%/armor.png						\
 %D%/async_multiple_provides.png			\
 %D%/block.png						\
 %D%/blocking_multiple_provides.png			\
 %D%/collateral0.png					\
 %D%/collateral1.png					\
 %D%/collateral_multiple_provides.png			\
 %D%/defer.png						\
 %D%/defer-cancel.png					\
 %D%/direct_in.png					\
 %D%/direct_multiple_out1.png				\
 %D%/direct_multiple_out2.png				\
 %D%/direct_out.png					\
 %D%/external_multiple_out1.png				\
 %D%/external_multiple_out2.png				\
 %D%/external_multiple_out3.png				\
 %D%/ihello-bool.png					\
 %D%/indirect_blocking_multiple_external_out.png	\
 %D%/indirect_blocking_out.png				\
 %D%/indirect_in.png					\
 %D%/indirect_multiple_out1.png				\
 %D%/indirect_multiple_out2.png				\
 %D%/indirect_multiple_out3.png				\
 %D%/indirect_out.png					\
 %D%/multiple_provides.png				\
 %D%/simple-state-machine.png				\
 %D%/some_system.png
