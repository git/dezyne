#! /bin/sh
# Dezyne --- Dezyne command line tools
#
# Copyright © 2019,2023,2024 Janneke Nieuwenhuizen <janneke@gnu.org>
# Copyright © 2020 Rutger van Beusekom <rutger@dezyne.org>
#
# This file is part of Dezyne.
#
# Dezyne is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Dezyne is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with Dezyne.  If not, see <http://www.gnu.org/licenses/>.
#
# Commentary:
#
# Code:

LANG=en_US.UTF-8
export LANG
for E in $(set | grep -oE LC_[^=]+); do unset $E; done

# Do not overwrite our config.h.in
AUTOHEADER=true
export AUTOHEADER

set -e
libtoolize --version
autoreconf -ifv

# Replace Automake's build-aux/mdate-sh with build-aux/mdate-from-git, our
# own, reproducible version.
chmod +w build-aux/mdate-sh
rm -f build-aux/mdate-sh
ln -s mdate-from-git.scm build-aux/mdate-sh
